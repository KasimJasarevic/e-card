﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using e_Card.Context.Implementation;
using e_Card.Models;
using e_Card.ViewModel;
using Microsoft.AspNet.Identity;

namespace e_Card.Services
{
    public class PatientService
    {
        private readonly ApplicationDbContext _db;

        public PatientService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        public IList<PatientVm> GetAll()
        {
            IList<PatientVm> result = _db.Patients.Where(u => u.IsActive).Select(d => new PatientVm
            {
                Name = d.Name,
                Surname = d.Surname,
                Gender = d.Gender,
                Birth = d.Birth,
                Nationality = d.Nationality,
                Address = d.Address,
                PatientId = d.Id,
                Email = d.Email,
                Height = d.Height,
                Weight = d.Weight,
                BloodType = d.BloodType,
                Jmbg = d.Jmbg
            }).ToList();

            return result;
        }

        public IEnumerable<PatientVm> Read()
        {
            return GetAll();
        }

        public bool CreatePatient(PatientVm model, ApplicationUserManager userManager)
        {
            var userName = model.Name.ToLower() + "." + model.Surname.ToLower();
            var user = new Patient
            {
                Name = model.Name,
                Surname = model.Surname,
                UserName = userName,
                Gender = model.Gender,
                MiddleName = model.MiddleName,
                CreatedDate = DateTime.Now,
                Birth = DateTime.Now,
                Nationality = model.Nationality,
                IsActive = true,
                EmailConfirmed = true,
                Address = model.Address,
                Email = model.Email,
                UrlImage = model.UrlImage,
                Weight = model.Weight,
                Height = model.Height,
                BloodType = model.BloodType,
                Jmbg = model.Jmbg,
            };

            var result = userManager.Create(user, model.Password);

            var currentUser = userManager.FindByName(user.UserName);

            if (result.Succeeded && currentUser != null)
            {
                var entity = new ECard
                {
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    FamilyDiseases = "Šećer",
                    SerialNo = model.Jmbg,
                    PatientId = currentUser.Id,
                };

                _db.ECards.Add(entity);
                _db.SaveChanges();
            }
          
            if (currentUser != null)
            {
                var roleresult = userManager.AddToRole(currentUser.Id, "Patient");
                return roleresult.Succeeded && result.Succeeded;
            }
            return false;
        }

        public bool UpdatePatient(PatientVm model, ApplicationUserManager userManager)
        {
            var user = (Patient)userManager.FindById(model.PatientId);

            var updateCard = user.Jmbg.Equals(model.Jmbg);

            user.Name = model.Name;
            user.Surname = model.Surname;
            user.Gender = model.Gender;
            user.UserName = model.Name.ToLower() + "." + model.Surname.ToLower();
            user.MiddleName = model.MiddleName;
            user.Birth = model.Birth;
            user.Nationality = model.Nationality;
            user.Address = model.Address;
            user.Email = model.Email;
            user.UrlImage = model.UrlImage;
            user.Height = model.Height;
            user.Weight = model.Weight;
            user.BloodType = model.BloodType;
            user.Jmbg = model.Jmbg;

            var result = userManager.Update(user);

            if (result.Succeeded && !updateCard)
            {
                var card = _db.ECards.FirstOrDefault(c => c.PatientId == user.Id);

                if (card != null)
                {
                    card.SerialNo = model.Jmbg;
                    card.UpdatedDate = DateTime.Now;
                    
                    _db.ECards.Attach(card);
                    _db.Entry(card).State = EntityState.Modified;
                    _db.SaveChanges();
                }
            }

            return result.Succeeded;
        }

        public bool DeletePatient(PatientVm model, ApplicationUserManager userManager)
        {
            var user = (Patient)userManager.FindById(model.PatientId);
            user.IsActive = false;

            var result = userManager.Update(user);

            return result.Succeeded;
        }
    }
}