﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using e_Card.Context.Implementation;
using e_Card.Hubs;
using e_Card.Models;
using e_Card.ViewModel;
using Kendo.Mvc.UI;
using Notification = e_Card.Models.Notification;

namespace e_Card.Services
{
    public class SchedulerTaskReviewService : ISchedulerEventService<TaskReviewVm>
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public virtual IQueryable<TaskReviewVm> GetAll()
        {
            return _db.TaskReviews.ToList().Select(task => new TaskReviewVm()
            {
                TaskId = task.Id,
                Title = task.Title,
                Start = task.Start,
                End = task.End,
                Description = task.Description,
                IsAllDay = task.IsAllDay,
                RecurrenceRule = task.RecurrenceRule,
                RecurrenceException = task.RecurrenceException,
                DoctorId = task.DoctorId,
                RequestId = task.RequestId
            }).AsQueryable();
        }

        public virtual IQueryable<TaskReviewVm> GetAllDoctorReview(string doctorId)
        {
            return _db.TaskReviews.ToList().Where(x => x.DoctorId == doctorId).Select(task => new TaskReviewVm()
            {
                TaskId = task.Id,
                Title = task.Title,
                Start = task.Start,
                End = task.End,
                Description = task.Description,
                IsAllDay = task.IsAllDay,
                RecurrenceRule = task.RecurrenceRule,
                RecurrenceException = task.RecurrenceException,
                DoctorId = task.DoctorId,
                RequestId = task.RequestId
            }).AsQueryable();
        }

        public virtual IQueryable<RequestTherapyVm> GetAllRequestTherapies(int departmentId)
        {
            return _db.RequestTherapies.ToList().Where(x => x.Review.Request.DepartmentId == departmentId 
                                                        && DateTime.Now >= x.DateFrom 
                                                        &&  DateTime.Now <= x.DateTo)
            .Select(task => new RequestTherapyVm()
            {
                ReviewId = task.ReviewId,
                TherapyName = task.Therapy.Name,
                Patient = task.Review.Request.Patient,
                DateFrom = task.DateFrom,
                DateTo = task.DateTo,
                Description = task.Description,
                DiseaseId = task.DiseaseId
            }).AsQueryable();
        }

        public void Insert(TaskReviewVm appointment, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }

        public void Update(TaskReviewVm appointment, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }

        public void Delete(TaskReviewVm appointment, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }

        public virtual void Insert(TaskReviewVm task, ModelStateDictionary modelState, int? requestId)
        {
            if (ValidateModel(task, modelState))
            {
                try
                {
                    if (requestId != null)
                    {
                        var review = new Review()
                        {
                            RequestId = requestId.Value,
                            DateReview = task.Start,
                            ComentByMedicalStaff = task.Description,
                            DoctorId = task.DoctorId
                        };

                        _db.Reviews.Add(review);
                        _db.SaveChanges();

                        var request = _db.Requests.FirstOrDefault(x => x.Id == requestId.Value && x.IsActive);

                        if (request != null)
                        {
                            request.IsActive = false;

                            _db.Requests.Attach(request);
                            _db.Entry(request).State = EntityState.Modified;
                            _db.SaveChanges();

                            var notification = new Notification()
                            {
                                PatientId = request.PatientId,
                                IsSeen = false,
                                Remark = "Naručeni ste " + request.PreferedDate.ToShortDateString() + " na odjel " +
                                         request.Department.Name
                            };

                            _db.Notifications.Add(notification);
                            _db.SaveChanges();

                            NotificationHub.SendToUser(notification.Remark, notification.Id, request.Patient.UserName);

                        }
                    }

                    if (string.IsNullOrEmpty(task.Title))
                    {
                        task.Title = "";
                    }

                    var entity = task.ToEntity();
                    if (requestId != null) entity.RequestId = requestId.Value;

                    _db.TaskReviews.Add(entity);
                    _db.SaveChanges();

                    task.TaskId = entity.Id;

                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public virtual void Update(TaskReviewVm task, ModelStateDictionary modelState, int? requestId)
        {
            if (ValidateModel(task, modelState))
            {
                try
                {
                    if (string.IsNullOrEmpty(task.Title))
                    {
                        task.Title = "";
                    }

                    var entity = _db.TaskReviews.Include("Doctor")
                        .FirstOrDefault(m => m.Id == task.TaskId);

                    if (entity != null)
                    {
                        entity.Title = task.Title;
                        entity.Start = task.Start;
                        entity.End = task.End;
                        entity.Description = task.Description;
                        entity.IsAllDay = task.IsAllDay;
                        entity.RecurrenceRule = task.RecurrenceRule;
                        entity.RecurrenceException = task.RecurrenceException;

                        var review = _db.Reviews.FirstOrDefault(x => x.RequestId == entity.RequestId && x.IsActive);

                        if (review != null)
                        {
                            review.RequestId = entity.RequestId;
                            review.DateReview = task.Start;
                            review.ComentByMedicalStaff = task.Description;
                            review.DoctorId = task.DoctorId;

                            _db.Reviews.Attach(review);
                            _db.Entry(review).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                    }

                    _db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public void Delete(TaskReviewVm task, ModelStateDictionary modelState, int? requestId)
        {

            var entity = _db.TaskReviews.FirstOrDefault(x => x.Id == task.TaskId && x.IsActive);

            if (entity != null)
            {
                var review = _db.Reviews.FirstOrDefault(x => x.RequestId == entity.RequestId && x.IsActive);
                if (review != null)
                {
                    review.IsActive = false;
                    _db.Reviews.Attach(review);
                    _db.Entry(review).State = EntityState.Modified;
                    _db.SaveChanges();
                }

                _db.TaskReviews.Attach(entity);
                _db.TaskReviews.Remove(entity);
                _db.SaveChanges();
            }
        }

        private bool ValidateModel(TaskReviewVm task, ModelStateDictionary modelState)
        {
            if (task.Start > task.End)
            {
                modelState.AddModelError("errors", @"End date must be greater or equal to Start date.");
                return false;
            }

            return true;
        }

        IList<TaskReviewVm> ISchedulerEventService<TaskReviewVm>.GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RequestTherapyVm> GetAllTherapies(int departmentId)
        {
            return GetAllRequestTherapies(departmentId);
        }
    }
}