﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using e_Card.Areas.AdministrationClinic.Controllers;
using e_Card.Context.Implementation;
using e_Card.Models;
using e_Card.ViewModel;

namespace e_Card.Services
{
    public class ECardService : BaseController
    {
        private readonly ApplicationDbContext _db;

        public ECardService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
        }

        public IList<ECardVm> GetAll()
        {
            IList<ECardVm> result = _db.ECards.Where(x => x.Patient.IsActive).Select(m => new ECardVm
            {
                CardId = m.Id,
                CreatedDate = m.CreatedDate,
                UpdatedDate = m.UpdatedDate,
                FamilyDiseases = m.FamilyDiseases,
                PatientId = m.PatientId,
                SerialNo = m.SerialNo,
                PatientName = m.Patient.Name,
            }).ToList();

            return result;
        }

        public IEnumerable<ECardVm> Read()
        {
            return GetAll();
        }

        #region CardDisease
        public IEnumerable<CardDiseaseVm> ReadCardDisease(int id)
        {
            return GetAllCardDisease(id);
        }

        private IEnumerable<CardDiseaseVm> GetAllCardDisease(int id)
        {
            IList<CardDiseaseVm> result = _db.CardDiseases.Where(x => x.CardId == id).Select(m => new CardDiseaseVm
            {
                CardId = id,
                DiseaseId = m.DiseaseId,
                DateFrom = m.DateFrom,
                DateTo = m.DateTo,
                Comment = m.Comment,
                DiseaseName = m.Disease.Name,
                IsActive = m.Disease.IsActive
            }).ToList();

            return result;
        }

        public string CreateCardDisease(CardDiseaseVm model)
        {
            try
            {
                var entity = new CardDisease
                {
                    CardId = model.CardId,
                    DiseaseId = model.DiseaseId,
                    DateFrom = DateTime.Now,
                    DateTo = model.DateTo,
                    Comment = model.Comment,
                    IsActive = true
                };

                _db.CardDiseases.Add(entity);
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string UpdateCardDisease(CardDiseaseVm model)
        {
            try
            {
                var target = _db.CardDiseases.FirstOrDefault(l => l.CardId == model.CardId && l.DiseaseId == model.DiseaseId);

                if (target != null)
                {
                    target.CardId = model.CardId;
                    target.DiseaseId = model.DiseaseId;
                    target.DateFrom = model.DateFrom;
                    target.DateTo = model.DateTo;
                    target.Comment = model.Comment;
                    _db.CardDiseases.Attach(target);
                    _db.Entry(target).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string DestroyCardDisease(CardDiseaseVm model)
        {
            try
            {
                var target =
                    _db.CardDiseases.FirstOrDefault(p => p.CardId == model.CardId && p.DiseaseId == model.DiseaseId);

                if (target != null)
                {
                    _db.CardDiseases.Attach(target);
                    _db.CardDiseases.Remove(target);
                    _db.SaveChanges();
                }
                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        #endregion

        #region CardMedicaments
        private IEnumerable<CardMedicamentVm> GetAllCardMedicaments(int id)
        {
            IList<CardMedicamentVm> result = _db.CardMedicaments.Where(x => x.CardId == id).Select(m => new CardMedicamentVm()
            {
                CardId = id,
                MedicamentId = m.MedicamentId,
                DateFrom = m.DateFrom,
                DateTo = m.DateTo,
                Comment = m.Comment,
                MedicamentName = m.Medicament.Name,
                IsActive = m.Medicament.IsActive
            }).ToList();

            return result;
        }

        public IEnumerable<CardMedicamentVm> ReadCardMedicament(int id)
        {
            return GetAllCardMedicaments(id);
        }

        public string CreateCardMedicaments(CardMedicamentVm model)
        {
            try
            {
                var entity = new CardMedicament
                {
                    CardId = model.CardId,
                    MedicamentId = model.MedicamentId,
                    DateFrom = DateTime.Now,
                    DateTo = model.DateTo,
                    Comment = model.Comment,
                    IsActive = true
                };

                _db.CardMedicaments.Add(entity);
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string UpdateCardMedicaments(CardMedicamentVm model)
        {
            try
            {
                var target = _db.CardMedicaments.FirstOrDefault(l => l.CardId == model.CardId && l.MedicamentId == model.MedicamentId);

                if (target != null)
                {
                    target.CardId = model.CardId;
                    target.MedicamentId = model.MedicamentId;
                    target.DateFrom = model.DateFrom;
                    target.DateTo = model.DateTo;
                    target.Comment = model.Comment;
                    _db.CardMedicaments.Attach(target);
                    _db.Entry(target).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string DestroyCardMedicaments(CardMedicamentVm model)
        {
            try
            {
                var target =
                    _db.CardMedicaments.FirstOrDefault(p => p.CardId == model.CardId && p.MedicamentId == model.MedicamentId);

                if (target != null)
                {
                    _db.CardMedicaments.Attach(target);
                    _db.CardMedicaments.Remove(target);
                    _db.SaveChanges();
                }
                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private IEnumerable<CardAllergiesVm> GetAllCardAllergies(int id)
        {
            IList<CardAllergiesVm> result = _db.CardAllergieses.Where(x => x.CardId == id).Select(m => new CardAllergiesVm
            {
                CardId = id,
                AllergieId = m.AllergieId,
                DateFrom = m.DateFrom,
                DateTo = m.DateTo,
                Comment = m.Comment,
                AllergieName = m.Allergies.Name,
                IsActive = m.Allergies.IsActive
            }).ToList();

            return result;
        }

        #endregion

        public IEnumerable<CardAllergiesVm> ReadCardAllergies(int id)
        {
            return GetAllCardAllergies(id);
        }

        public string CreateCardAllergies(CardAllergiesVm model)
        {
            try
            {
                var entity = new CardAllergies()
                {
                    CardId = model.CardId,
                    AllergieId = model.AllergieId,
                    DateFrom = DateTime.Now,
                    DateTo = model.DateTo,
                    Comment = model.Comment,
                };

                _db.CardAllergieses.Add(entity);
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string UpdateCardAllergies(CardAllergiesVm model)
        {
            try
            {
                var target = _db.CardAllergieses.FirstOrDefault(l => l.CardId == model.CardId && l.AllergieId == model.AllergieId);

                if (target != null)
                {
                    target.CardId = model.CardId;
                    target.AllergieId = model.AllergieId;
                    target.DateFrom = model.DateFrom;
                    target.DateTo = model.DateTo;
                    target.Comment = model.Comment;
                    _db.CardAllergieses.Attach(target);
                    _db.Entry(target).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string DestroyCardAllergies(CardAllergiesVm model)
        {

            return "";
        }
    }
}