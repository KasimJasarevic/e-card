﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using e_Card.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace e_Card.Context.Implementation
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            builder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public IDbSet<Medicaments> Medicamentses { get; set; }

        public IDbSet<Diseases> Diseaseses { get; set; }

        public IDbSet<Department> Departments { get; set; }
         
        public IDbSet<Laboratory> Laboratories { get; set; }

        public IDbSet<LaboratoryReport> LaboratoryReports { get; set; }

        public IDbSet<Patient> Patients { get; set; }

        public IDbSet<Doctor> Doctors { get; set; }

        public IDbSet<LaboratoryRequest> LaboratoryRequests { get; set; }

        public IDbSet<LaboratoryWorker> LaboratoryWorkers { get; set; }

        public IDbSet<Perscription> Perscriptions { get; set; }
        public IDbSet<Test> Tests { get; set; }

        public IDbSet<Notification> Notifications { get; set; }

        public IDbSet<Clinic> Clinics { get; set; }

        public IDbSet<ECard> ECards { get; set; }

        public IDbSet<DepartmentClinic> DepartmentClinics { get; set; }
        public IDbSet<LaboratoryMedicalReport> LaboratoryMedicalReports { get; set; }

        public IDbSet<Type> Types { get; set; }

        public IDbSet<Request> Requests { get; set; }

        public IDbSet<Review> Reviews { get; set; }

        public IDbSet<Therapy> Therapies { get; set; }

        public IDbSet<PatientReview> PatientReviews { get; set; }

        public IDbSet<RequestTherapy> RequestTherapies { get; set; }

        public IDbSet<Diagnosis> Diagnoses { get; set; }

        public IDbSet<TestDone> TestDones { get; set; }

        public IDbSet<CardDisease> CardDiseases { get; set; }

        public IDbSet<CardMedicament> CardMedicaments { get; set; }

        public IDbSet<Allergies> Allergies { get; set; }

        public IDbSet<CardAllergies> CardAllergieses { get; set; }

        public IDbSet<MedicalStaff> MedicalStaff { get; set; }

        public IDbSet<TaskReview> TaskReviews { get; set; }

        public IDbSet<LabReports> LabReportses { get; set; }
    }
}