﻿using System.Data.Entity;
using e_Card.Models;

namespace e_Card.Context
{
    public interface IApplicationDbContext
    {
        IDbSet<Medicaments> Medicamentses { get; set; }

        IDbSet<Diseases> Diseaseses { get; set; }

        IDbSet<Department> Departments { get; set; }   

        IDbSet<Laboratory> Laboratories { get; set; } 

        IDbSet<Patient> Patients { get; set; } 

        IDbSet<Doctor> Doctors { get; set; } 

        IDbSet<LaboratoryReport> LaboratoryReports { get; set; }
        
        IDbSet<LaboratoryRequest> LaboratoryRequests { get; set; }

        IDbSet<LaboratoryWorker> LaboratoryWorkers { get; set; }

        IDbSet<Perscription> Perscriptions { get; set; } 

        IDbSet<Test> Tests { get; set; }
        IDbSet<Notification> Notifications { get; set; }

        IDbSet<Clinic> Clinics { get; set; }

        IDbSet<ECard> ECards { get; set; }

        IDbSet<DepartmentClinic> DepartmentClinics { get; set; }

        IDbSet<LaboratoryMedicalReport> LaboratoryMedicalReports { get; set; }

        IDbSet<Request> Requests { get; set; }

        IDbSet<Review> Reviews { get; set; } 

        IDbSet<Therapy> Therapies { get; set; }

        IDbSet<PatientReview> PatientReviews { get; set; }

        IDbSet<RequestTherapy> RequestTherapies { get; set; }

        IDbSet<Diagnosis> Diagnoses { get; set; }

        IDbSet<TestDone> TestDones { get; set; }

        IDbSet<CardDisease> CardDiseases { get; set; }

        IDbSet<CardMedicament> CardMedicaments { get; set; }

        IDbSet<Allergies> Allergies { get; set; }

        IDbSet<CardAllergies> CardAllergieses { get; set; }

        IDbSet<MedicalStaff> MedicalStaff { get; set; }

        IDbSet<TaskReview> TaskReviews { get; set; }

        IDbSet<LabReports> LabReportses { get; set; }
    }
}
