﻿function error_handler(e) {
    if (e.errors) {
        var message = "Errors:\n";
        $.each(e.errors, function (key, value) {
            if ('errors' in value) {
                $.each(value.errors, function () {
                    message += this + "\n";
                });
            }
        });
        var notification = $("#notification").data("kendoNotification");

        notification.show({
            message: message
        }, "error");
        // alert(message);
    }
}