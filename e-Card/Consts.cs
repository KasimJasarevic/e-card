﻿namespace e_Card
{
    public class Consts
    {
        public static string Create = "CreateUser";

        public static string Update = "UpdateUser";

        public static string Delete = "DeleteUser";

        public static string ComboBoxDepartment = "ComboBoxDepartment";
    }
}