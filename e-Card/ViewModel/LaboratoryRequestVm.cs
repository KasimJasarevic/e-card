﻿using System.Collections.Generic;
using e_Card.Models;

namespace e_Card.ViewModel
{
    public class LaboratoryRequestVm
    {
        public int? LaboratoryRequestId { get; set; }

        public int ReviewId { get; set; }

        public List<int> LaboratoryReportIds { get; set; }

        public string PatientId { get; set; }

        public Patient Patient { get; set; }

        public string LaboratoryWorkerId { get; set; }

        public string CommentDoctor { get; set; }

        public string CommentLaboratoryWorker { get; set; }

        public string File { get; set; }

        public bool Status { get; set; } = false;

        public int LaboratoryId { get; set; }

        public Laboratory Laboratory { get; set; }

        public ICollection<LaboratoryReport> LaboratoryReports { get; set; }
    }
}