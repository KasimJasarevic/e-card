﻿using System.Collections.Generic;
using e_Card.Models;

namespace e_Card.ViewModel
{
    public class LaboratoryReportRequestVm
    {
        public int LaboratoryRequestId { get; set; }

        public string Label { get; set; }

        public double MinValue { get; set; }

        public double MaxValue { get; set; }

        public double AverageValue { get; set; }

        public string Comment { get; set; }

        public List<LaboratoryReport> LaboratoryReports { get; set; }
    }
}