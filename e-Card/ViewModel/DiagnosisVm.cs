﻿namespace e_Card.ViewModel
{
    public class DiagnosisVm
    {
        public int DiagnosisId  { get; set; }

        public int ReviewId { get; set; }

        public int DiseaseDiagnosisId { get; set; }

        public RequestTherapyVm RequestTherapyVm { get; set; }

        public string Description { get; set; }

        public string ClinicalStatus { get; set; }

        public string PatientId { get; set; }

        public string Advice { get; set; }

        public AllergieVm AllergieVm { get; set; }
    }
}