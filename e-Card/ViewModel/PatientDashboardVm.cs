﻿using System.Collections.Generic;
using e_Card.Models;

namespace e_Card.ViewModel
{
    public class PatientDashboardVm : PatientVm
    {
        public RequestVm RequestVm { get; set; }

        public List<RequestTherapy> Therapies { get; set; } = new List<RequestTherapy>();

        public List<CardMedicament> Medicamentses { get; set; } = new List<CardMedicament>();
    }
}