﻿using e_Card.Models;

namespace e_Card.ViewModel
{
    public class PatientReviewVm
    {
        public Doctor Doctor { get; set; }

        public Clinic Clinic { get; set; }

        public PercriptionVm PercriptionVm { get; set; }

        public virtual Patient Patient { get; set; }

        public int ReviewId { get; set; }

        public LaboratoryRequestVm LaboratoryRequestVm { get; set; }

        public DiagnosisVm DiagnosisVm { get; set; }
    }
}