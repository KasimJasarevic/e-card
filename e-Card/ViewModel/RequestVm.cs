﻿using System;
using System.ComponentModel.DataAnnotations;

namespace e_Card.ViewModel
{
    public class RequestVm
    {
        public int RequestId { get; set; }
        
        [Required]
        public string Description { get; set; }

        [Required]
        public string ShortDescription { get; set; }

        public DateTime DateRequest { get; set; }

        public DateTime PreferedDate { get; set; }

        public bool Approved { get; set; }

        public int DepartmentId { get; set; }

        public string PatientId { get; set; }

        public string PatientName { get; set; }

        public string DoctorId { get; set; }

        public int ClinicId { get; set; }
    }
}