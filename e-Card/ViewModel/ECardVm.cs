﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_Card.Models;

namespace e_Card.ViewModel
{
    public class ECardVm
    {
        public int CardId { get; set; }

        public string PatientId { get; set; }

        public string FamilyDiseases { get; set; }

        public string SerialNo { get; set; }

        public string PatientName { get; set; }

        public List<CardDisease> Diseases { get; set; }

        public List<CardMedicament> Medicaments { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}