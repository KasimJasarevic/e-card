﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace e_Card.ViewModel
{
    public class PatientVm : UserVm
    {
        public string PatientId { get; set; }

        [Required]
        public string BloodType { get; set; }

        public double Height { get; set; }

        public double Weight { get; set; }

        public string Jmbg { get; set; }

        public List<int> DiseaseIds { get; set; }

        public List<int> MedicamentIds { get; set; }
    }
}