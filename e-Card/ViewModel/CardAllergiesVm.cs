﻿namespace e_Card.ViewModel
{
    public class CardAllergiesVm : CardHistoryVm
    {
        public int CardId { get; set; }

        public int AllergieId { get; set; }

        public string AllergieName { get; set; }

        public bool IsActive { get; set; }
    }
}