﻿using System;
using System.ComponentModel.DataAnnotations;

namespace e_Card.ViewModel
{
    public class AllergieVm
    {
        public int AllergieId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string PatientId { get; set; }

        public DateTime DateFromAllergie { get; set; }

        public DateTime? DateTo { get; set; }
    }
}