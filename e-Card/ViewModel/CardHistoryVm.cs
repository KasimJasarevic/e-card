﻿using System;
using System.ComponentModel.DataAnnotations;

namespace e_Card.ViewModel
{
    public class CardHistoryVm
    {
        public string Comment { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateFrom { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DateTo { get; set; }
    }
}