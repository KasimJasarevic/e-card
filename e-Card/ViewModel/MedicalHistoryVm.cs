﻿using System;

namespace e_Card.ViewModel
{
    public class MedicalHistoryVm
    {
        public int? CardId { get; set; }

        public string FamilyDisease { get; set; }

        public string UrlImage { get; set; }

        public string PatientName { get; set; }

        public string PatientSurname { get; set; }

        public string Address { get; set; }

        public string BloodType { get; set; }

        public double? Weight { get; set; }

        public double? Height { get; set; }

        public string Profession { get; set; }

        public string Jmbg { get; set; }

        public DateTime? PatientBirth { get; set; }

        public int DiseasesCount { get; set; }

        public int MedicamentsCount { get; set; }

        public int AllergiesCount { get; set; }
    }
}