﻿namespace e_Card.ViewModel
{
    public class CardMedicamentVm : CardHistoryVm
    {
        public int CardId { get; set; }

        public int MedicamentId { get; set; }

        public string MedicamentName { get; set; }

        public bool IsActive { get; set; }
    }
}