﻿using System;
using System.ComponentModel.DataAnnotations;
using e_Card.Models;

namespace e_Card.ViewModel
{
    public class RequestTherapyVm
    {
        public int ReviewId { get; set; }

        [Required]
        public string TherapyName { get; set; }

        public int DiseaseId { get; set; }

        [Required]
        public DateTime DateFrom { get; set; }

        [Required]
        public DateTime DateTo { get; set; }

        [Required]
        public string Description { get; set; }

        public string PatientId { get; set; }

        public Patient Patient { get; set; }
    }
}