﻿using System;
using System.ComponentModel.DataAnnotations;

namespace e_Card.ViewModel
{
    public class UserVm
    {
        public UserVm()
        {
            IsActive = true;
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Gender { get; set; }

        public string MiddleName { get; set; }

        [DataType(DataType.Date)]
        public DateTime? CreatedDate { get; set; }

        public string FullName { get; set; }

        [DataType(DataType.Date)]
        public DateTime Birth { get; set; }

        [Required]
        public bool IsActive { get; set ; }

        [Required]
        public string Nationality { get; set; }

        [Required]
        public string Address { get; set; }

        public string Password { get; set; }

        [Required]
        public string Email { get; set; }

        public string UrlImage { get; set; }

        public string Guid { get; set; }
    }
}