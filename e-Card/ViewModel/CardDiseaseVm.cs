﻿using System;

namespace e_Card.ViewModel
{
    public class CardDiseaseVm : CardHistoryVm
    {
        public int CardId { get; set; }

        public int DiseaseId { get; set; }

        public string DiseaseName { get; set; }

        public bool IsActive { get; set; }
    }
}