﻿using System.ComponentModel.DataAnnotations;

namespace e_Card.ViewModel
{
    public class PercriptionVm
    {
        public int PercriptionId { get; set; }

        [Required]
        public string ConsumptionWay { get; set; }

        public int Quantity { get; set; }

        public int MedicamentId { get; set; }

        public int ReviewId { get; set; }

        public string PatientId { get; set; }

        [Required]
        public string Note { get; set; }
    }
}