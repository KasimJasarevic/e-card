﻿using System;
using System.ComponentModel.DataAnnotations;
using e_Card.Models;
using Kendo.Mvc.UI;

namespace e_Card.ViewModel
{
    public class TaskReviewVm : ISchedulerEvent
    {
        public int TaskId { get; set; }

        public int RequestId { get; set; }

        [Required]
        public string DoctorId { get; set; }

        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsAllDay { get; set; } = false;

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }

        public virtual MedicalStaff MedicalStaff { get; set; }

        public RequestVm RequestVm { get; set; }

        public TaskReview ToEntity()
        {
            var task = new TaskReview()
            {
                Id = TaskId,
                Title = Title,
                Start = Start,
                End = End,
                Description = Description,
                IsAllDay = IsAllDay,
                RecurrenceRule = RecurrenceRule,
                RecurrenceException = RecurrenceException,
                DoctorId = DoctorId
            };

            return task;
        }
    }
}