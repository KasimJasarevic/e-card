﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(e_Card.Startup))]
namespace e_Card
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
