﻿using System.ComponentModel.DataAnnotations;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Test : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Label { get; set; }

        [Required]
        public string Unit { get; set; }
    }
}