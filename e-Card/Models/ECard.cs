﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class ECard : Entity<int>
    {
        [Index("IX_First", 1, IsUnique = true)]
        public string PatientId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string FamilyDiseases { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Required]
        public string SerialNo { get; set; }

        [ForeignKey("PatientId")]
        public virtual Patient Patient { get; set; }

        public ICollection<CardDisease> Diseases { get; set; }

        public ICollection<CardMedicament> Medicaments { get; set; }

        public ICollection<CardAllergies> Allergieses { get; set; }
    }
}