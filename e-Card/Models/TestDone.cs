﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class TestDone
    {
        [Column(Order = 0)]
        [Key, ForeignKey("Test")]
        public int TestId { get; set; }

        public virtual Test Test { get; set; }

        [Column(Order = 1)]
        [Key, ForeignKey("Review")]
        public int ReviewId { get; set; }
        
        public Review Review { get; set; }

        [Required]
        public string Value { get; set; }
    }
}