﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    [Table("LaboratoryWorker")]
    public class LaboratoryWorker : ApplicationUser
    {
        public int LaboratoryId { get; set; }

        [ForeignKey("LaboratoryId")]
        public Laboratory Laboratory { get; set; }

        public DateTime DateOfEmployment { get; set; }
    }
}