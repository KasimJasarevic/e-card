﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Clinic : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Phone { get; set; }

        public string Email { get; set; }

        public int? NumberOfEmploye { get; set; }

        public string Url { get; set; }

        public virtual ICollection<DepartmentClinic> Departments { get; set; }
    }
}