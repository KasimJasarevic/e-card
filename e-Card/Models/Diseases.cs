﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Diseases : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        public string LatinName { get; set; }

        public string Description { get; set; }

        [Required]
        public bool GeneticDisease { get; set; }

        [Required]
        public bool InfectiousDisease { get; set; }

        public ICollection<CardDisease> Cards { get; set; }

        public ICollection<CardAllergies> Allergieses { get; set; }

        public ICollection<CardMedicament> Medicaments { get; set; }
    }
}