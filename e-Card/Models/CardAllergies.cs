﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class CardAllergies
    {
        [Column(Order = 0)]
        [Key, ForeignKey("Card")]
        public int CardId { get; set; }

        [Column(Order = 1)]
        [Key, ForeignKey("Allergies")]
        public int AllergieId { get; set; }

        public string Comment { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public virtual Allergies Allergies { get; set; }

        public virtual ECard Card { get; set; }
    }
}