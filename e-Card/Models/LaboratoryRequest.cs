﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class LaboratoryRequest : Entity<int>
    {
        public int ReviewId { get; set; }

        [ForeignKey("ReviewId")]
        public Review Review { get; set; }

        public string LaboratoryWorkerId { get; set; }

        [ForeignKey("LaboratoryWorkerId")]
        public LaboratoryWorker LaboratoryWorker { get; set; }

        public string CommentDoctor { get; set; }

        public string CommentLaboratoryWorker { get; set; }

        public string File { get; set; }

        public bool Status { get; set; }

        public int? LaboratoryId { get; set; }   

        public virtual ICollection<LaboratoryReport> LaboratoryReports { get; set; }
    }
}