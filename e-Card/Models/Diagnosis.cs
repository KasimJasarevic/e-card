﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Diagnosis : Entity<int>
    {
        [ForeignKey("Review")]
        public int ReviewId { get; set; }

        public virtual Review Review { get; set; }

        [ForeignKey("Disease")]
        public int DiseaseId { get; set; }
        
        public virtual Diseases Disease { get; set; }

        public string Description { get; set; }

        public string Treatment { get; set; }

        public string ClinicalStatus { get; set; }

        public string Advice { get; set; }

        public ICollection<RequestTherapy> Therapies { get; set; }
    }
}