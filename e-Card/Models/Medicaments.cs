﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Medicaments : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        public double Grammage { get; set; }

        public string Description { get; set; }

        public int TypeId { get; set; }

        [ForeignKey("TypeId")]
        public Type Type { get; set; }

        public ICollection<CardMedicament> Cards { get; set; }
    }
}