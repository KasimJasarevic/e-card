﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Therapy : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        public ICollection<RequestTherapy> Requests { get; set; }
    }
}