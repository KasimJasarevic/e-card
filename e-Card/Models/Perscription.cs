﻿using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Perscription : Entity<int>
    {
        public int Quantity { get; set; }

        public string ConsumptionWay { get; set; }

        public int MedicamentId { get; set; }

        [ForeignKey("MedicamentId")]
        public Medicaments Medicaments { get; set; }

        public int ReviewId { get; set; }

        [ForeignKey("ReviewId")]
        public Review Review { get; set; }
    }
}