﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class PatientReview
    {
        [Key, ForeignKey("Review")]
        public int ReviewId { get; set; }

        public virtual Review Review { get; set; }

        public DateTime DatePatientReview { get; set; }

        public string Status { get; set; }

        public string PatientId { get; set; }

        public string DoctorId { get; set; }

        [ForeignKey("DoctorId")]
        public Doctor Doctor { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }
    }
}