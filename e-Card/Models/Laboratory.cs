﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Laboratory : Entity<int>
    {
        [ForeignKey("Clinic")]
        public int ClinicId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual Clinic Clinic { get; set; }

        public ICollection<LaboratoryMedicalReport> LaboratoryReport { get; set; }
    }
}