﻿using System.ComponentModel.DataAnnotations;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class LaboratoryItem : Entity<int>
    {
        //public string Amylase { get; set; }

        //public string Albumin { get; set; }

        //public string Feritin { get; set; }

        //public string TotalBillirubin { get; set; }

        //public string Creatinine { get; set; }

        //public string Urea { get; set; }

        //public string Glucose { get; set; }

        //public string UricAcid { get; set; }

        //public string Cholesterol { get; set; }

        //public string Triglycerides { get; set; }

        //public string Glucoze { get; set; }

        //public string Lactate { get; set; }

        //public string TestStrip { get; set; }

        //public string UrineSediment { get; set; }

        //public string Comment { get; set; }

        //public string DocumentPath { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Label { get; set; }

        public int AverageValue { get; set; }

        public int MaxValue { get; set; }

        public int MinValue { get; set; }
    }
}