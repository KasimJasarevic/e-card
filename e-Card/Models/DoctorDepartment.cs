﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class DoctorDepartment
    {
        [Column(Order = 0)]
        [Key, ForeignKey("Doctor")]
        public string DoctorId { get; set; }

        [Column(Order = 1)]
        [Key, ForeignKey("Department")]
        public int DepartmentId { get; set; }

        public virtual Doctor Doctor { get; set; }

        public virtual Department Department { get; set; }
    }
}