﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class CardDisease
    {
        [Column(Order = 0)]
        [Key, ForeignKey("Card")]
        public int CardId { get; set; }

        [Column(Order = 1)]
        [Key, ForeignKey("Disease")]
        public int DiseaseId { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string Comment { get; set; }

        public virtual Diseases Disease { get; set; }

        public virtual ECard Card { get; set; }

        public bool IsActive { get; set; }
    }
}