﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class LaboratoryMedicalReport
    {
        [Column(Order = 0)]
        [Key, ForeignKey("Laboratory")]
        public int LaboratoryId { get; set; }

        [Column(Order = 1)]
        [Key, ForeignKey("LaboratoryReport")]
        public int LaboratoryReportId { get; set; }

        public virtual Laboratory Laboratory { get; set; }

        public virtual LaboratoryReport LaboratoryReport { get; set; }

        public bool IsActive { get; set; } = true;
    }
}