﻿using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class TaskReview : Entity<int>
    {
        public System.DateTime Start { get; set; }
        public System.DateTime End { get; set; }

        public int RequestId { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsAllDay { get; set; } = false;
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }
        public string DoctorId { get; set; }

        [ForeignKey("DoctorId")]
        public Doctor Doctor { get; set; }
    }
}