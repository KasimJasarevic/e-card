﻿using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class LabReports : Entity<int>
    {
        public int LaboratoryReportId { get; set; }

        public int RequestId { get; set; }

        public string Value { get; set; }
    }
}