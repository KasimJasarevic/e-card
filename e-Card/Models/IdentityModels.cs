﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace e_Card.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);

            // Add custom user claims here
            userIdentity.AddClaim(new Claim("FullName", FullName));
            userIdentity.AddClaim(new Claim("RegistrationDate", CreatedDate.ToString()));

            return userIdentity;
        }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(128)]
        public string Surname { get; set; }

        public string Gender { get; set; }

        public int? RoleId { get; set; }

        public string RoleName { get; set; }

        public string MiddleName { get; set; }

        public DateTime? CreatedDate { get; set; } = DateTime.Today;

        public DateTime? LastLoginDate { get; set; }

        [NotMapped]
        public string FullName => $"{Name} {Surname}";

        public DateTime Birth { get; set; }

        public string UrlImage { get; set; }

        public bool IsActive { get; set; }

        public string Nationality { get; set; }

        public string Address { get; set; }
    }
}