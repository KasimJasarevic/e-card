﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class RequestTherapy
    {
        [Column(Order = 0)]
        [Key, ForeignKey("Review")]
        public int ReviewId { get; set; }

        public virtual Review Review { get; set; }

        [Column(Order = 2)]
        [Key, ForeignKey("Therapy")]
        public int TherapyId { get; set; }

        public virtual Therapy Therapy { get; set; }

        [Column(Order = 3)]

        [Key, ForeignKey("Diseases")]
        public int DiseaseId { get; set; }

        public virtual Diseases Diseases { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public string Description { get; set; }
    }
}