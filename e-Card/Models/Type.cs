﻿using System.ComponentModel.DataAnnotations;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Type : Entity<int>
    {
        [Required]
        public string Name { get; set; }
    }
}