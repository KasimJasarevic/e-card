﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class DepartmentClinic
    {
        [Column(Order = 1)]
        [Key, ForeignKey("Clinic")]
        public int ClinicId { get; set; }

        [Column(Order = 0)]
        [Key, ForeignKey("Department")]
        public int DepartmentId { get; set; }

        public virtual Clinic Clinic { get; set; }

        public virtual Department Department { get; set; }

        public DateTime? DateOfEstablishment { get; set; }

        public string Description { get; set; }

        public string ChiefId { get; set; }

        [ForeignKey("ChiefId")]
        public virtual Doctor Doctor { get; set; }

        public bool IsActive { get; set; } = true;
    }
}