﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    [Table("MedicalStaff")]
    public class MedicalStaff : ApplicationUser
    {
        [Index("IX_First", 1, IsUnique = true)]
        public int ClinicId { get; set; }

        [ForeignKey("ClinicId")]
        public Clinic Clinic { get; set; }

        public int DepartmentId { get; set; }

        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        public DateTime DateOfEmployment { get; set; }

        [Required]
        public string Profession { get; set; }
    }
}