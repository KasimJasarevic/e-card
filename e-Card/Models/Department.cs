﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Department : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<DepartmentClinic> Clinics { get; set; }
    }
}