﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    [Table("Patient")]
    public class Patient : ApplicationUser
    {
        [Required]
        public string BloodType { get; set; }

        public double Height { get; set; }

        public double Weight { get; set; }

        public string Jmbg { get; set; }
    }
}