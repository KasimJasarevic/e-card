﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_Card.Models
{
    public class Review
    {
        [Key, ForeignKey("Request")]
        public int RequestId { get; set; }

        public string DoctorId { get; set; }

        [ForeignKey("DoctorId")]
        public Doctor Doctor { get; set; }

        public DateTime DateReview { get; set; }

        public virtual Request Request { get; set; }

        public bool IsActive { get; set; } = true;

        public string ComentByMedicalStaff { get; set; }

        public virtual PatientReview PatientReview { get; set; }
    }
}