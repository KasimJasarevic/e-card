﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class LaboratoryReport : Entity<int>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Label { get; set; }

        public double AverageValue { get; set; }

        public int MaxValue { get; set; }

        public int MinValue { get; set; }

        public ICollection<LaboratoryMedicalReport> Laboratories { get; set; }
    }
}