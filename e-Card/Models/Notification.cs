﻿using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Notification : Entity<int>
    {
       
        public string PatientId { get; set; }

        [ForeignKey("PatientId")]
        public virtual Patient Patient { get; set; }

        public Review Review { get; set; }

        public string Remark { get; set; }

        public bool IsSeen { get; set; } = false;
    }
}