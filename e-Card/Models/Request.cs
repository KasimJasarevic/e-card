﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e_Card.Interfaces.Implementation;

namespace e_Card.Models
{
    public class Request : Entity<int>
    {
        [Required]
        public DateTime DateRequest { get; set; }

        [Required]
        public DateTime PreferedDate { get; set; }

        public string Description { get; set; }

        public bool Approved { get; set; } = false;

        [Required]
        public string ShortDescription { get; set; }

        public int DepartmentId { get; set; }

        public int ClinicId { get; set; }

        public string PatientId { get; set; }

        [ForeignKey("PatientId")]
        public virtual Patient Patient { get; set; }

        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }
    }
}