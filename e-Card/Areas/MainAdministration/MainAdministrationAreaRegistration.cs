﻿using System.Web.Mvc;

namespace e_Card.Areas.MainAdministration
{
    public class MainAdministrationAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "MainAdministration";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MainAdministration_default",
                "MainAdministration/{controller}/{action}/{id}",
                new { controller = "Clinics", action = "Index", area = "MainAdministration", id = UrlParameter.Optional } // Parameter defaults
                //new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}