﻿using System.Web.Mvc;

namespace e_Card.Areas.AdministrationClinic
{
    public class AdministrationClinicAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "AdministrationClinic";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AdministrationClinic_default",
                "AdministrationClinic/{controller}/{action}/{id}",
                new { controller = "Doctors", action = "Index", area = "AdministrationClinic", id = UrlParameter.Optional } // Parameter defaults
                //new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}