﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class MedicamentVm
    {
        public int MedicamentId { get; set; }

        [Required]
        public string Name { get; set; }

        public double Grammage { get; set; }

        public string Description { get; set; }

        public int TypeId { get; set; }

        public string TypeName { get; set; }
    }
}