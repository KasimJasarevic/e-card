﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class DepartmentVm
    {
        public int DepartmentId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string ClinicName { get; set; }

        public string ChiefId { get; set; }

        public DateTime? DateOfEstablishment { get; set; }

        public List<int> ClinicIds { get; set; }
    }
}