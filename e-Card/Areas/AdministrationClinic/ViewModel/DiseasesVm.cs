﻿using System.ComponentModel.DataAnnotations;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class DiseasesVm
    {
        public int DiseasesId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string LatinName { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public bool GeneticDisease { get; set; }

        [Required]
        public bool InfectiousDisease { get; set; }
    }
}