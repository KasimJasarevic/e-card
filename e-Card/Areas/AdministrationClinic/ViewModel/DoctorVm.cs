﻿using System;
using System.ComponentModel.DataAnnotations;
using e_Card.Models;
using e_Card.ViewModel;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class DoctorVm : UserVm
    {
        public string DoctorId { get; set; }

        public int ClinicId { get; set; }

        public int DepartmentId { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateOfEmployment { get; set; }

        [Required(ErrorMessage = "Please enter a proffesion")]
        public string Profession { get; set; }

        public Clinic Clinic { get; set; }
    }
}