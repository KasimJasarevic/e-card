﻿using System;
using System.ComponentModel.DataAnnotations;
using e_Card.Models;
using e_Card.ViewModel;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class LaboratoryWorkerVm : UserVm
    {
        public string LaboratoryWorkerId { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateOfEmployment { get; set; }

        public int LaboratoryId { get; set; }

        public Laboratory Laboratory { get; set; }
    }
}