﻿using System.ComponentModel.DataAnnotations;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class TypeMedicamentsVm
    {
        public int TypeId { get; set; }

        [Required]
        public string Name { get; set; }
    }
}