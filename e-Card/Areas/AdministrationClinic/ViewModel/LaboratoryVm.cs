﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class LaboratoryVm
    {
        public int LaboratoryId { get; set; }

        public string ClinicName { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public int ClinicId { get; set; }

        public int ClinicIdUpdate { get; set; }

        public List<int> LaboratoryReportIds { get; set; }
    }
}