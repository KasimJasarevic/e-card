﻿using System.ComponentModel.DataAnnotations;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class LabItemVm
    {
        public int LabItemId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Label { get; set; }

        public double AverageValue { get; set; }

        public int MaxValue { get; set; }

        public int MinValue { get; set; }
    }
}