﻿using System.ComponentModel.DataAnnotations;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class AllergiesVm
    {
        public int AllergieId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}