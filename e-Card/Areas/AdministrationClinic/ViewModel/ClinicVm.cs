﻿using System.ComponentModel.DataAnnotations;

namespace e_Card.Areas.AdministrationClinic.ViewModel
{
    public class ClinicVm
    {
        public int ClinicId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public int? NumberOfEmploye { get; set; }

        public string Url { get; set; }
    }
}