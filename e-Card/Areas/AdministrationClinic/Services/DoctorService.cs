﻿using System;
using System.Collections.Generic;
using System.Linq;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;
using Microsoft.AspNet.Identity;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class DoctorService
    {
        private readonly ApplicationDbContext _db;

        public DoctorService(ApplicationDbContext context)
        {
            _db = context;
        }

        public IList<DoctorVm> GetAll()
        {
            IList<DoctorVm> result = _db.Doctors.Where(u => u.IsActive).Select(d => new DoctorVm()
            {
                Name = d.Name,
                Surname = d.Surname,
                Gender = d.Gender,
                Birth = d.Birth,
                Nationality = d.Nationality,
                Address = d.Address,
                Profession = d.Profession,
                DoctorId = d.Id,
                Email = d.Email,
                DateOfEmployment = d.DateOfEmployment
            }).ToList();

            return result;
        }

        public IEnumerable<DoctorVm> Read()
        {
            return GetAll();
        }

        internal bool DeleteDoctor(DoctorVm model, ApplicationUserManager userManager)
        {
            var user = (Doctor)userManager.FindById(model.DoctorId);
            user.IsActive = false;

            var result = userManager.Update(user);

            return result.Succeeded;
        }

        internal bool UpdateDoctor(DoctorVm model, ApplicationUserManager userManager)
        {
            var user = (Doctor)userManager.FindById(model.DoctorId);

            if (user != null)
            {
                user.Name = model.Name;
                user.Surname = model.Surname;
                user.Gender = model.Gender;
                user.UserName = model.Name.ToLower() + "." + model.Surname.ToLower();
                user.MiddleName = model.MiddleName;
                user.Birth = model.Birth;
                user.Nationality = model.Nationality;
                user.Address = model.Address;
                user.ClinicId = model.ClinicId;
                user.Email = model.Email;
                user.UrlImage = model.UrlImage;
                user.Profession = model.Profession;
                user.DepartmentId = model.DepartmentId;
                user.DateOfEmployment = model.DateOfEmployment;
            }

            var result = userManager.Update(user);

            return result.Succeeded;
        }

        internal bool CreateDoctor(DoctorVm model, ApplicationUserManager userManager)
        {
            var userName = model.Name.ToLower() + "." + model.Surname.ToLower();
            var doctor = new Doctor
            {
                Name = model.Name,
                Surname = model.Surname,
                UserName = userName,
                Gender = model.Gender,
                MiddleName = model.MiddleName,
                CreatedDate = DateTime.Now,
                Birth = DateTime.Now,
                Nationality = model.Nationality,
                IsActive = true,
                EmailConfirmed = true,
                Address = model.Address,
                Profession = model.Profession,
                ClinicId = model.ClinicId,
                Email = model.Email,
                UrlImage = model.UrlImage,
                DateOfEmployment = model.DateOfEmployment,
                DepartmentId = model.DepartmentId
                //Departments = model.DepartmentsId.Select(d => new DoctorDepartment
                //{
                //    DepartmentId =  d
                //}).ToList()
            };

            var result = userManager.Create(doctor, model.Password);

            var currentUser = userManager.FindByName(doctor.UserName);
            if (currentUser != null)
            {
                var roleresult = userManager.AddToRole(currentUser.Id, "Doctor");
                return roleresult.Succeeded && result.Succeeded;
            }
            return false;
        }
    }
}