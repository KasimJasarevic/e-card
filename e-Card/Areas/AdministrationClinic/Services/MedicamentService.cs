﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using e_Card.Areas.AdministrationClinic.Controllers;
using e_Card.Models;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class MedicamentService : BaseController
    {
        private readonly ApplicationDbContext _db;

        public MedicamentService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
        }

        public IList<MedicamentVm> GetAll()
        {
            IList<MedicamentVm> result = _db.Medicamentses.Select(m => new MedicamentVm
            {
                MedicamentId = m.Id,
                Name = m.Name,
                Description = m.Description,
                Grammage = m.Grammage,
                TypeId = m.TypeId,
                TypeName = m.Type.Name
            }).ToList();

            return result;
        }

        public IEnumerable<MedicamentVm> Read()
        {
            return GetAll();
        }

        public string Create(MedicamentVm model)
        {
            try
            {
                var first = GetAll().OrderByDescending(e => e.MedicamentId).FirstOrDefault();
                var id = first?.MedicamentId ?? 0;

                model.MedicamentId = id + 1;

                GetAll().Insert(0, model);

                var entity = new Medicaments
                {
                    Name = model.Name,
                    Description = model.Description,
                    Grammage = model.Grammage,
                    TypeId = model.TypeId
                };

                _db.Medicamentses.Add(entity);
                _db.SaveChanges();

                model.MedicamentId = entity.Id;

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Update(MedicamentVm model)
        {
            try
            {
                var target = One(e => e.MedicamentId == model.MedicamentId);

                if (target != null)
                {
                    target.Name = model.Name;
                    target.Description = model.Description;
                    target.Grammage = model.Grammage;
                    target.TypeId = model.TypeId;
                }

                var entity = new Medicaments
                {
                    Id = model.MedicamentId,
                    Name = model.Name,
                    Description = model.Description,
                    Grammage = model.Grammage,
                    TypeId = model.TypeId
                };

                _db.Medicamentses.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Destroy(MedicamentVm model)
        {
            try
            {
                var target = GetAll().FirstOrDefault(p => p.MedicamentId == model.MedicamentId);

                if (target != null)
                {
                    GetAll().Remove(target);
                }

                var entity = new Medicaments { Id = model.MedicamentId };

                _db.Medicamentses.Attach(entity);
                _db.Medicamentses.Remove(entity);
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public MedicamentVm One(Func<MedicamentVm, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }

        public JsonResult GetAllMedicaments(string text)
        {
            var results = _db.Medicamentses
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                results = results.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(results.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllAvailableMedicaments(string text, int? id)
        {
            var results = (from s in _db.Medicamentses
                where !(from sa in _db.CardMedicaments
                    where sa.MedicamentId.Equals(s.Id) && sa.CardId.Equals(id.Value)
                    select sa.MedicamentId).Contains(s.Id) && s.IsActive
                select new
                {
                    s.Id,
                    s.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                results = results.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(results.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}