﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using e_Card.Areas.AdministrationClinic.Controllers;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class AllergiesService : BaseController
    {
        private readonly ApplicationDbContext _db;

        public AllergiesService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        public IList<AllergiesVm> GetAll()
        {
            IList<AllergiesVm> result = _db.Allergies.Where(x => x.IsActive).Select(t => new AllergiesVm
            {
                AllergieId = t.Id,
                Name = t.Name,
                Description = t.Description
            }).ToList();

            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        public IEnumerable<AllergiesVm> Read()
        {
            return GetAll();
        }

        public string Create(AllergiesVm model)
        {
            try
            {
                var entity = new Allergies
                {
                    Name = model.Name,
                    Description = model.Description
                };

                _db.Allergies.Add(entity);
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Update(AllergiesVm model)
        {
            try
            {
                var target = _db.Allergies.FirstOrDefault(x => x.Id == model.AllergieId);

                if (target != null)
                {
                    target.Name = model.Name;
                    target.Description = model.Description;
                    target.Id = model.AllergieId;
                }

                _db.Allergies.Attach(target);
                _db.Entry(target).State = EntityState.Modified;
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Destroy(AllergiesVm model)
        {
            try
            {
                var target = _db.Allergies.FirstOrDefault(x => x.Id == model.AllergieId);

                if (target != null)
                {
                    target.IsActive = false;

                    _db.Allergies.Attach(target);
                    _db.Entry(target).State = EntityState.Modified;
                    _db.SaveChanges();
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public JsonResult GetAllAvailableAllergies(string text, int? id)
        {
            var results = (from s in _db.Allergies
                where !(from sa in _db.CardAllergieses
                    where sa.AllergieId.Equals(s.Id) && sa.CardId.Equals(id.Value)
                    select sa.AllergieId).Contains(s.Id) && s.IsActive
                select new
                {
                    s.Id,
                    s.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                results = results.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(results.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}