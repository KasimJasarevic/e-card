﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class ClinicServices
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public IList<ClinicVm> GetAll()
        {
            IList<ClinicVm> result = _db.Clinics.Where(x => x.IsActive).Select(clinic => new ClinicVm()
            {
                ClinicId = clinic.Id,
                Name = clinic.Name,
                Address = clinic.Address,
                Email = clinic.Email,
                Phone = clinic.Phone,
                NumberOfEmploye = clinic.NumberOfEmploye,
                Url = clinic.Url
            }).ToList();

            return result;
        }

        /// <summary>
        /// Function to read all clinics
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ClinicVm> Read()
        {
            return GetAll();
        }

        public string Create(ClinicVm clinic)
        {
            try
            {
                var first = GetAll().OrderByDescending(e => e.ClinicId).FirstOrDefault();
                var id = first?.ClinicId ?? 0;

                clinic.ClinicId = id + 1;

                GetAll().Insert(0, clinic);

                var clinicItem = new Clinic
                {
                    Name = clinic.Name,
                    Address = clinic.Address,
                    Phone = clinic.Phone,
                    Email = clinic.Email,
                    Url = clinic.Url
                };

                _db.Clinics.Add(clinicItem);
                _db.SaveChanges();

                clinic.ClinicId = clinicItem.Id;
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return string.Empty;
        }

        public string Update(ClinicVm clinic)
        {
            try
            {
                var target = One(e => e.ClinicId == clinic.ClinicId);

                if (target != null)
                {
                    target.Name = clinic.Name;
                    target.Address = clinic.Address;
                }

                var entity = new Clinic
                {
                    Id = clinic.ClinicId,
                    Name = clinic.Name,
                    Address = clinic.Address,
                    Phone = clinic.Phone,
                    Email = clinic.Email,
                    NumberOfEmploye = clinic.NumberOfEmploye,
                    Url = clinic.Url
                };

                _db.Clinics.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return string.Empty;
        }

        public string Destroy(ClinicVm clinic)
        {
            try
            {
                var target = _db.Clinics.FirstOrDefault(x => x.Id == clinic.ClinicId);

                if (target != null)
                {
                    target.IsActive = false;

                    _db.Clinics.Attach(target);
                    _db.Entry(target).State = EntityState.Modified;
                    _db.SaveChanges();

                    var result = _db.DepartmentClinics.Where(d => d.ClinicId == clinic.ClinicId && d.IsActive).ToList();

                    foreach (var item in result)
                    {
                        item.IsActive = false;

                        _db.DepartmentClinics.Attach(item);
                        _db.Entry(item).State = EntityState.Modified;
                        _db.SaveChanges();
                    }
                    _db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return string.Empty;
        }

        public ClinicVm One(Func<ClinicVm, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }
    }
}