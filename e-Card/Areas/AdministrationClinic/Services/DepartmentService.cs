﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class DepartmentService
    {
        private readonly ApplicationDbContext _db;

        public DepartmentService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        public IList<DepartmentVm> GetAll()
        {
            IList<DepartmentVm> result = _db.Departments.Where(x => x.IsActive).Select(d => new DepartmentVm
            {
                DepartmentId = d.Id,
                Name = d.Name,
                Description = d.Description,
                ClinicIds = d.Clinics.Select(s => s.ClinicId).ToList(),
                DateOfEstablishment = d.Clinics.FirstOrDefault(x => x.DepartmentId == d.Id).DateOfEstablishment,
                ChiefId = d.Clinics.FirstOrDefault(x => x.DepartmentId == d.Id).ChiefId
            }).ToList();

            return result;
        }

        public IEnumerable<DepartmentVm> Read()
        {
            return GetAll();
        }

        public string Create(DepartmentVm model)
        {
            try
            {
                var first = GetAll().OrderByDescending(e => e.DepartmentId).FirstOrDefault();
                var id = first?.DepartmentId ?? 0;

                model.DepartmentId = id + 1;

                var entity = new Department
                {
                    Name = model.Name,
                    Description = model.Description,
                    Clinics = model.ClinicIds.Select(d => new DepartmentClinic
                    {
                        ClinicId = d,
                        DateOfEstablishment = DateTime.Now,
                        Description = "Description",
                        ChiefId = model.ChiefId
                    }).ToList()
                };

                _db.Departments.Add(entity);
                _db.SaveChanges();

                GetAll().Insert(0, model);

                model.DepartmentId = entity.Id;
                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Destroy(DepartmentVm model)
        {
            try
            {
                var target = _db.Departments.FirstOrDefault(p => p.Id == model.DepartmentId);

                if (target != null)
                {
                    var result = _db.DepartmentClinics.Where(d => d.DepartmentId == model.DepartmentId && d.IsActive).ToList();

                    foreach (var item in result)
                    {
                        item.IsActive = false;
                        //_db.DepartmentClinics.Attach(item);
                        //_db.DepartmentClinics.Remove(item);
                        _db.DepartmentClinics.Attach(item);
                        _db.Entry(item).State = EntityState.Modified;
                        _db.SaveChanges();
                    }
                    _db.SaveChanges();

                    _db.Departments.Attach(target);
                    _db.Departments.Remove(target);
                    _db.SaveChanges();
                    GetAll().Remove(GetAll().FirstOrDefault(p => p.DepartmentId == model.DepartmentId));
                }
                return string.Empty;

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Update(DepartmentVm model)
        {
            try
            {
                var target = _db.Departments.FirstOrDefault(d => d.Id == model.DepartmentId);

                if (target != null)
                {
                    target.Name = model.Name;
                    target.Description = model.Description;
                }

                var result = _db.DepartmentClinics.Where(d => d.DepartmentId == model.DepartmentId && d.IsActive).ToList();

                foreach (var item in result)
                {
                    _db.DepartmentClinics.Attach(item);
                    _db.DepartmentClinics.Remove(item);
                }
                _db.SaveChanges();

                if (model.ClinicIds != null && target != null)
                {
                    target.Clinics = model.ClinicIds.Select(c => new DepartmentClinic()
                    {
                        ClinicId = c,
                        DepartmentId = model.DepartmentId,
                        DateOfEstablishment = model.DateOfEstablishment,
                        ChiefId = model.ChiefId,
                        Description = "Description"
                    }).ToList();
                }

                _db.Departments.Attach(target);
                _db.Entry(target).State = EntityState.Modified;
                _db.SaveChanges();
                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            } 
        }

        public DepartmentVm One(Func<DepartmentVm, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }

        /// <summary>
        /// Free memory
        /// </summary>
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}