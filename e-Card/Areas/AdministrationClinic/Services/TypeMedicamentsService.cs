﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Type = e_Card.Models.Type;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class TypeMedicamentsService : IDisposable
    {
        private readonly ApplicationDbContext _db;

        public TypeMedicamentsService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        public IList<TypeMedicamentsVm> GetAll()
        {
            IList<TypeMedicamentsVm> result = _db.Types.Where(x => x.IsActive).Select(t => new TypeMedicamentsVm
            {
                TypeId = t.Id,
                Name = t.Name,
            }).ToList();

            return result;
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public IEnumerable<TypeMedicamentsVm> Read()
        {
            return GetAll();
        }

        public string Create(TypeMedicamentsVm model)
        {
            try
            {
                var first = GetAll().OrderByDescending(e => e.TypeId).FirstOrDefault();
                var id = first?.TypeId ?? 0;

                model.TypeId = id + 1;

                GetAll().Insert(0, model);

                var entity = new Type
                {
                    Name = model.Name

                };

                _db.Types.Add(entity);
                _db.SaveChanges();

                model.TypeId = entity.Id;

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Update(TypeMedicamentsVm model)
        {
            try
            {
                var target = One(e => e.TypeId == model.TypeId);

                if (target != null)
                {
                    target.Name = model.Name;
                }

                var entity = new Type
                {
                    Id = model.TypeId,
                    Name = model.Name
                };

                _db.Types.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Destroy(TypeMedicamentsVm model)
        {
            try
            {
                var target = _db.Types.FirstOrDefault(x => x.Id == model.TypeId);

                if (target != null)
                {
                    target.IsActive = false;
                    _db.Types.Attach(target);
                    _db.Entry(target).State = EntityState.Modified;
                    _db.SaveChanges();
                  
                }
                
                return string.Empty;

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public TypeMedicamentsVm One(Func<TypeMedicamentsVm, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }
    }
}