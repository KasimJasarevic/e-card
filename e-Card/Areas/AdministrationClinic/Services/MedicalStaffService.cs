﻿using System;
using System.Collections.Generic;
using System.Linq;
using e_Card.Areas.AdministrationClinic.Controllers;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;
using e_Card.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class MedicalStaffService : BaseController
    {
        private readonly ApplicationDbContext _db;

        public MedicalStaffService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        public IList<MedicalStaffVm> GetAll()
        {
            IList<MedicalStaffVm> result = _db.MedicalStaff.Where(u => u.IsActive).Select(d => new MedicalStaffVm()
            {
                Name = d.Name,
                Surname = d.Surname,
                Gender = d.Gender,
                Birth = d.Birth,
                Nationality = d.Nationality,
                Address = d.Address,
                Profession = d.Profession,
                MedicalStaffId = d.Id,
                Email = d.Email,
                DateOfEmployment = d.DateOfEmployment
            }).ToList();

            return result;
        }

        public IEnumerable<MedicalStaffVm> Read()
        {
            return GetAll();
        }

        internal bool DeleteMedicalStaff(MedicalStaffVm model, ApplicationUserManager userManager)
        {
            var user = (MedicalStaff) userManager.FindById(model.MedicalStaffId);
            user.IsActive = false;

            var result = userManager.Update(user);

            return result.Succeeded;
        }

        internal bool UpdateMedicalStaff(MedicalStaffVm model, ApplicationUserManager userManager)
        {
            var user = (MedicalStaff) userManager.FindById(model.MedicalStaffId);

            if (user != null)
            {
                user.Name = model.Name;
                user.Surname = model.Surname;
                user.Gender = model.Gender;
                user.UserName = model.Name.ToLower() + "." + model.Surname.ToLower();
                user.MiddleName = model.MiddleName;
                user.Birth = model.Birth;
                user.Nationality = model.Nationality;
                user.Address = model.Address;
                user.ClinicId = model.ClinicId;
                user.Email = model.Email;
                user.UrlImage = model.UrlImage;
                user.Profession = model.Profession;
                user.DepartmentId = model.DepartmentId;
                user.DateOfEmployment = model.DateOfEmployment;
            }

            var result = userManager.Update(user);

            return result.Succeeded;
        }

        internal bool CreateMedicalStaff(MedicalStaffVm model, ApplicationUserManager userManager)
        {
            var userName = model.Name.ToLower() + "." + model.Surname.ToLower();
            var entity = new MedicalStaff
            {
                Name = model.Name,
                Surname = model.Surname,
                UserName = userName,
                Gender = model.Gender,
                MiddleName = model.MiddleName,
                CreatedDate = DateTime.Now,
                Birth = DateTime.Now,
                Nationality = model.Nationality,
                IsActive = true,
                EmailConfirmed = true,
                Address = model.Address,
                Profession = model.Profession,
                ClinicId = model.ClinicId,
                Email = model.Email,
                UrlImage = model.UrlImage,
                DateOfEmployment = model.DateOfEmployment,
                DepartmentId = model.DepartmentId
            };

            var result = userManager.Create(entity, model.Password);

            var currentUser = userManager.FindByName(entity.UserName);
            if (currentUser != null)
            {
                var roleresult = userManager.AddToRole(currentUser.Id, "MedicalStaff");
                return roleresult.Succeeded && result.Succeeded;
            }
            return false;
        }

        [Authorize]
        public IList<RequestVm> GetAllRequestsMethod(string userId, bool approved)
        {
            if (string.IsNullOrEmpty(userId))
            {
                userId = string.Empty;
            }
            var user = _db.MedicalStaff.FirstOrDefault(x => x.Id == userId);
            if (user == null)
            {
                return new List<RequestVm>();
            }

            IList<RequestVm> result = _db.Requests.Where(x => x.IsActive && x.DepartmentId == user.DepartmentId && x.ClinicId == user.ClinicId && x.Approved == approved).Select(
                r => new RequestVm()
                {
                    Description = r.Description,
                    ShortDescription = r.ShortDescription,
                    PreferedDate = r.PreferedDate,
                    DateRequest = r.DateRequest,
                    DepartmentId = r.DepartmentId,
                    PatientId = r.PatientId,
                    ClinicId = r.ClinicId,
                    RequestId = r.Id,
                    Approved = r.Approved,
                    PatientName = r.Patient.Name + r.Patient.Surname
                }).ToList();

            if (approved)
            {
                result = result.Where(x => x.PreferedDate.Equals(DateTime.Today)).ToList();
            }

            return result;
        }

        public IEnumerable<RequestVm> GetAllRequests(string userId, bool approved)
        {
            return GetAllRequestsMethod(userId, approved);
        }
    }
}