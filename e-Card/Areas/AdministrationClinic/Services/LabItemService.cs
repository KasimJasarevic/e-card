﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class LabItemService : IDisposable
    {
        private readonly ApplicationDbContext _db;

        public LabItemService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        public IList<LabItemVm> GetAll()
        {
            IList<LabItemVm> result = _db.LaboratoryReports.Where(x => x.IsActive).Select(lab => new LabItemVm
            {
                LabItemId = lab.Id,
                Name = lab.Name,
                Label = lab.Label,
                AverageValue = lab.AverageValue,
                MinValue = lab.MinValue,
                MaxValue = lab.MaxValue,
            }).ToList();

            return result;
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public IEnumerable<LabItemVm> Read()
        {
            return GetAll();
        }

        public string Create(LabItemVm model)
        {
            try
            {
                var first = GetAll().OrderByDescending(e => e.LabItemId).FirstOrDefault();
                var id = first?.LabItemId ?? 0;

                model.LabItemId = id + 1;

                GetAll().Insert(0, model);

                var entity = new LaboratoryReport
                {
                    Name = model.Name,
                    Label = model.Label,
                    AverageValue = model.AverageValue,
                    MinValue = model.MinValue,
                    MaxValue = model.MaxValue,
                };

                _db.LaboratoryReports.Add(entity);
                _db.SaveChanges();

                model.LabItemId = entity.Id;

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Update(LabItemVm model)
        {
            try
            {
                var target = _db.LaboratoryReports.FirstOrDefault(e => e.Id == model.LabItemId);

                if (target != null)
                {
                    target.Name = model.Name;
                    target.Label = model.Label;
                    target.AverageValue = model.AverageValue;
                    target.MinValue = model.MinValue;
                    target.MaxValue = model.MaxValue;
                }

                _db.LaboratoryReports.Attach(target);
                _db.Entry(target).State = EntityState.Modified;
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public LabItemVm One(Func<LabItemVm, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }

        public string Destroy(LabItemVm model)
        {
            try
            {
                var target = _db.LaboratoryReports.FirstOrDefault(p => p.Id == model.LabItemId);

                if (target != null)
                {
                    var result = _db.LaboratoryMedicalReports
                        .Where(d => d.LaboratoryReportId == model.LabItemId && d.IsActive).ToList();

                    foreach (var item in result)
                    {
                        item.IsActive = false;

                        _db.LaboratoryMedicalReports.Attach(item);
                        _db.Entry(item).State = EntityState.Modified;
                    }
                    _db.SaveChanges();
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}