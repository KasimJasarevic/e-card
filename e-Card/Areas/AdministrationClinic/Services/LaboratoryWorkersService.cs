﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;
using Microsoft.AspNet.Identity;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class LaboratoryWorkersService
    {
        private readonly ApplicationDbContext _db;

        public LaboratoryWorkersService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        public IList<LaboratoryWorkerVm> GetAll()
        {
            IList<LaboratoryWorkerVm> result = _db.LaboratoryWorkers.Where(u => u.IsActive).Select(d => new LaboratoryWorkerVm()
            {
                Name = d.Name,
                Surname = d.Surname,
                Gender = d.Gender,
                Birth = d.Birth,
                Nationality = d.Nationality,
                Address = d.Address,
                LaboratoryId = d.LaboratoryId,
                LaboratoryWorkerId = d.Id,
                Email = d.Email,
                DateOfEmployment = d.DateOfEmployment
            }).ToList();

            return result;
        }

        public IEnumerable<LaboratoryWorkerVm> Read()
        {
            return GetAll();
        }

        internal bool CreateLaboratoryWorker(LaboratoryWorkerVm model, ApplicationUserManager userManager)
        {
            var userName = model.Name.ToLower() + "." + model.Surname.ToLower();
            var entity = new LaboratoryWorker
            {
                Name = model.Name,
                Surname = model.Surname,
                UserName = userName,
                Gender = model.Gender,
                MiddleName = model.MiddleName,
                CreatedDate = DateTime.Now,
                Birth = DateTime.Now,
                Nationality = model.Nationality,
                IsActive = true,
                EmailConfirmed = true,
                Address = model.Address,
                LaboratoryId = model.LaboratoryId,
                Email = model.Email,
                UrlImage = model.UrlImage,
                DateOfEmployment = model.DateOfEmployment,
            };

            var result = userManager.Create(entity, model.Password);

            var currentUser = userManager.FindByName(entity.UserName);
            if (currentUser != null)
            {
                var roleresult = userManager.AddToRole(currentUser.Id, "LaboratoryWorker");
                return roleresult.Succeeded && result.Succeeded;
            }
            return false;
        }

        internal bool UpdateLaboratoryWorker(LaboratoryWorkerVm model, ApplicationUserManager userManager)
        {
            var user = (LaboratoryWorker)userManager.FindById(model.LaboratoryWorkerId);

            if (user != null)
            {
                user.Name = model.Name;
                user.Surname = model.Surname;
                user.Gender = model.Gender;
                user.UserName = model.Name.ToLower() + "." + model.Surname.ToLower();
                user.MiddleName = model.MiddleName;
                user.Birth = model.Birth;
                user.Nationality = model.Nationality;
                user.Address = model.Address;
                user.LaboratoryId = model.LaboratoryId;
                user.Email = model.Email;
                user.UrlImage = model.UrlImage;
                user.DateOfEmployment = model.DateOfEmployment;
            }

            var result = userManager.Update(user);

            return result.Succeeded;
        }

        internal bool DeleteLaboratoryWorker(LaboratoryWorkerVm model, ApplicationUserManager userManager)
        {
            var user = (LaboratoryWorker)userManager.FindById(model.LaboratoryWorkerId);
            user.IsActive = false;

            var result = userManager.Update(user);

            return result.Succeeded;
        }
    }
}