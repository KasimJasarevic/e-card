﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class DiseaseService : IDisposable
    {
        private readonly ApplicationDbContext _db;

        public DiseaseService(ApplicationDbContext context)
        {
            _db = context;
        }

        /// <summary>
        /// Get all diseases
        /// </summary>
        /// <returns></returns>
        public IList<DiseasesVm> GetAll()
        {
            IList<DiseasesVm> result = _db.Diseaseses.Select(disease => new DiseasesVm()
            {
                DiseasesId = disease.Id,
                Name = disease.Name,
                LatinName = disease.LatinName,
                Description = disease.Description,
                GeneticDisease = disease.GeneticDisease,
                InfectiousDisease = disease.InfectiousDisease
            }).ToList();


            return result;
        }

        /// <summary>
        /// Function to read diseases
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DiseasesVm> Read()
        {
            return GetAll();
        }

        /// <summary>
        /// Function to createDiseases
        /// </summary>
        /// <param name="diseases"></param>
        public string Create(DiseasesVm diseases)
        {
            try
            {
                var first = GetAll().OrderByDescending(e => e.DiseasesId).FirstOrDefault();
                var id = first?.DiseasesId ?? 0;

                diseases.DiseasesId = id + 1;

                GetAll().Insert(0, diseases);

                var entity = new Diseases
                {
                    Name = diseases.Name,
                    Description = diseases.Description,
                    LatinName = diseases.LatinName,
                    GeneticDisease = diseases.GeneticDisease,
                    InfectiousDisease = diseases.InfectiousDisease
                };

                _db.Diseaseses.Add(entity);
                _db.SaveChanges();

                diseases.DiseasesId = entity.Id;
                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Function to update diseases
        /// </summary>
        /// <param name="diseases"></param>
        public string Update(DiseasesVm diseases)
        {
            try
            {
                var target = One(e => e.DiseasesId == diseases.DiseasesId);

                if (target != null)
                {
                    target.Name = diseases.Name;
                    target.Description = diseases.Description;
                    target.LatinName = diseases.LatinName;
                    target.GeneticDisease = diseases.GeneticDisease;
                    target.InfectiousDisease = diseases.InfectiousDisease;
                }

                var entity = new Diseases
                {
                    Id = diseases.DiseasesId,
                    Name = diseases.Name,
                    Description = diseases.Description,
                    LatinName = diseases.LatinName,
                    GeneticDisease = diseases.GeneticDisease,
                    InfectiousDisease = diseases.InfectiousDisease
                };

                _db.Diseaseses.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Function to delete diseases
        /// </summary>
        /// <param name="diseases"></param>
        public string Destroy(DiseasesVm diseases)
        {
            try
            {
                var target = GetAll().FirstOrDefault(p => p.DiseasesId == diseases.DiseasesId);

                if (target != null)
                {
                    GetAll().Remove(target);
                }

                var entity = new Diseases { Id = diseases.DiseasesId };

                _db.Diseaseses.Attach(entity);
                _db.Diseaseses.Remove(entity);
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
            
        }

        public DiseasesVm One(Func<DiseasesVm, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }

        /// <summary>
        /// Free memory
        /// </summary>
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}