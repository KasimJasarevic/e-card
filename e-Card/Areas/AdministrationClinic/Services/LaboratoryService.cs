﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;

namespace e_Card.Areas.AdministrationClinic.Services
{
    public class LaboratoryService : IDisposable
    {
        private readonly ApplicationDbContext _db;

        public LaboratoryService(ApplicationDbContext applicationDbContext)
        {
            _db = applicationDbContext;
        }

        public IList<LaboratoryVm> GetAll()
        {
            IList<LaboratoryVm> result = _db.Laboratories.Where(x => x.IsActive).Select(lab => new LaboratoryVm
            {
                LaboratoryId = lab.Id,
                Name = lab.Name,
                Description = lab.Description,
                ClinicName = lab.Clinic.Name,
                ClinicId = lab.ClinicId,
                LaboratoryReportIds = lab.LaboratoryReport.Select(l => l.LaboratoryReportId).ToList()
            }).ToList();


            return result;
        }

        public IEnumerable<LaboratoryVm> Read()
        {
            return GetAll();
        }

        public string Create(LaboratoryVm model)
        {
            try
            {
                var first = GetAll().OrderByDescending(e => e.LaboratoryId).FirstOrDefault();
                var id = first?.LaboratoryId ?? 0;

                model.LaboratoryId = id + 1;

                GetAll().Insert(0, model);

                var entity = new Laboratory
                {
                    Name = model.Name,
                    Description = model.Description,
                    ClinicId = model.ClinicId,
                    LaboratoryReport = model.LaboratoryReportIds.Select(l => new LaboratoryMedicalReport
                    {
                        LaboratoryReportId = l
                    }).ToList()
                };

                _db.Laboratories.Add(entity);
                _db.SaveChanges();

                model.LaboratoryId = entity.Id;

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Update(LaboratoryVm model)
        {
            try
            {
                var target = _db.Laboratories.FirstOrDefault(l => l.Id == model.LaboratoryId);

                if (target != null)
                {
                    target.Name = model.Name;
                    target.Description = model.Description;
                    target.ClinicId = model.ClinicId;

                    var result = _db.LaboratoryMedicalReports.Where(l => l.LaboratoryId == model.LaboratoryId && l.IsActive).ToList();

                    foreach (var item in result)
                    {
                        _db.LaboratoryMedicalReports.Attach(item);
                        _db.LaboratoryMedicalReports.Remove(item);
                    }
                    _db.SaveChanges();
                }

                if (target != null && model.LaboratoryReportIds != null)
                {
                    target.LaboratoryReport = model.LaboratoryReportIds.Select(l => new LaboratoryMedicalReport
                    {
                        LaboratoryReportId = l
                    }).ToList();
                }

                _db.Laboratories.Attach(target);
                _db.Entry(target).State = EntityState.Modified;
                _db.SaveChanges();

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Destroy(LaboratoryVm model)
        {
            try
            {
                var target = _db.Laboratories.FirstOrDefault(p => p.Id == model.LaboratoryId);

                if (target != null)
                {
                    var result = _db.LaboratoryMedicalReports.Where(d => d.LaboratoryId == model.LaboratoryId && d.IsActive).ToList();

                    foreach (var item in result)
                    {
                        item.IsActive = false;

                        _db.LaboratoryMedicalReports.Attach(item);
                        _db.Entry(item).State = EntityState.Modified;
                    }
                    _db.SaveChanges();
                }
                return string.Empty;

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public LaboratoryVm One(Func<LaboratoryVm, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}