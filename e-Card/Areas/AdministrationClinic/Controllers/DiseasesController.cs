﻿using System.Web.Mvc;
using System.Web.WebPages;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Linq;
using e_Card.Services;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class DiseasesController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly DiseaseService _diseaseService = new DiseaseService(new ApplicationDbContext());

        [Authorize(Roles = "Admin")]
        // GET: AdministrationClinic/Diseases
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_diseaseService.Read().ToDataSourceResult(request));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Create([DataSourceRequest] DataSourceRequest request, DiseasesVm model)
        {
            if (model != null && ModelState.IsValid)
            {
               var success = _diseaseService.Create(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, DiseasesVm model)
        {
            if (model != null && ModelState.IsValid)
            {
               var success = _diseaseService.Update(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, DiseasesVm model)
        {
            if (model != null)
            {
                var success = _diseaseService.Destroy(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetAll(string text)
        {
            var results = _db.Diseaseses
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                results = results.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(results.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllAvailable(string text, int? id)
        {
            var results = (from s in _db.Diseaseses
                where !(from sa in _db.CardDiseases
                    where sa.DiseaseId.Equals(s.Id) && sa.CardId.Equals(id.Value)
                    select sa.DiseaseId).Contains(s.Id) && s.IsActive
                select new
                {
                    s.Id,
                    s.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                results = results.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(results.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}