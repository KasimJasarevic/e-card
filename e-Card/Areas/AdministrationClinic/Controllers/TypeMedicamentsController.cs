﻿using System;
using System.Web.Mvc;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Linq;
using System.Web.WebPages;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class TypeMedicamentsController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly TypeMedicamentsService _typeMedicamentsService = new TypeMedicamentsService(new ApplicationDbContext());

        [Authorize(Roles = "Admin")]
        // GET: AdministrationClinic/Diseases
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_typeMedicamentsService.Read().ToDataSourceResult(request));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, TypeMedicamentsVm model)
        {
            if (model != null && ModelState.IsValid)
            {
                var success = _typeMedicamentsService.Create(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, TypeMedicamentsVm model)
        {
            if (model != null && ModelState.IsValid)
            {
                var success = _typeMedicamentsService.Update(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, TypeMedicamentsVm model)
        {
            if (model != null)
            {
                var success = _typeMedicamentsService.Destroy(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetAll(string text)
        {
            var result = (from t in _db.Types
                              //where !_db.Medicamentses.Select(m => m.TypeId).Contains(t.Id)
                          where t.IsActive
                          select new
                          {
                              t.Id,
                              t.Name
                          });

            if (!String.IsNullOrEmpty(text))
            {
                result = result.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(result.Take(10), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}