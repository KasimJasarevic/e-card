﻿using System;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Controllers;
using e_Card.Helpers;
using e_Card.Models;
using e_Card.ViewModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class MedicalStaffAdministrationController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly MedicalStaffService _medicalStaffService = new MedicalStaffService(new ApplicationDbContext());

        public ActionResult Index()
        {
            return View(new MedicalStaffVm());
        }

        public ActionResult Create()
        {
            return View("Create", new MedicalStaffVm());
        }

        public ActionResult Update(string id)
        {
            var target = _db.Users.OfType<MedicalStaff>().FirstOrDefault(d => d.Id == id && d.IsActive);

            if (target != null)
            {
                var doctorVm = new MedicalStaffVm
                {
                    Name = target.Name,
                    Surname = target.Surname,
                    Email = target.Email,
                    Gender = target.Gender,
                    MiddleName = target.MiddleName,
                    Birth = target.Birth,
                    Nationality = target.Nationality,
                    Address = target.Address,
                    Profession = target.Profession,
                    MedicalStaffId = target.Id,
                    UrlImage = target.UrlImage,
                    DepartmentId = target.DepartmentId,
                    DateOfEmployment = target.DateOfEmployment,
                    ClinicId = target.ClinicId
                };

                return View("Update", doctorVm);
            }
            return HttpNotFound();
        }

        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_medicalStaffService.Read().ToDataSourceResult(request));
        }

        public string Destroy(RequestVm model)
        {
            try
            {
                var target = _db.Requests.FirstOrDefault(x => x.Id == model.RequestId);

                if (target != null)
                {
                    target.IsActive = false;

                    _db.Requests.Attach(target);
                    _db.Entry(target).State = EntityState.Modified;
                    _db.SaveChanges();
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public ActionResult GetAllRequests([DataSourceRequest] DataSourceRequest request, bool approved)
        {
            var userId = User.Identity.GetUserId();
            return Json(_medicalStaffService.GetAllRequests(userId, approved).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicalStaffHandler([DataSourceRequest] DataSourceRequest request, MedicalStaffVm model, string status)
        {
            var success = false;
            if (model != null && ModelState.IsValid)
            {
                var accountController =
                    new AccountController {ControllerContext = ControllerContext};

                success = accountController.MedicalStaffHandler(model, status);
            }

            return RedirectToAction(success ? "Index" : "Create");
        }

        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase image)
        {
            if (image != null)
            {
                var extension = Path.GetExtension(image.FileName)?.ToLower();
                if (extension != ".jpg" && extension != ".png" && extension != ".jpeg")
                    return Json(new {success = false, error = "Wrong photo extension, please try with another photo."});

                Stream stream = image.InputStream;
                Image img = Image.FromStream(stream);
                var guid = Guid.NewGuid();

                var fileName = guid + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/UserImages"), fileName);

                //bool size300 = img.Height > 200 || img.Width < 200;

                //if (size300) return Json(new { success = false, message = "Photo dimensions should be minimum 200x200." });

                var myArray = (byte[]) new ImageConverter().ConvertTo(img, typeof(byte[]));
                FileHelpers.SaveFile(myArray, physicalPath);

                return Json(new {success = true, path = "/UserImages/" + fileName, name = fileName});
            }

            return Json(new {success = false});
        }

        public JsonResult GetAll(string text)
        {

            var result = _db.Users.OfType<MedicalStaff>()
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!String.IsNullOrEmpty(text))
            {
                result = result.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(result.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}