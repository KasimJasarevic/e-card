﻿using System.Web.Mvc;
using System.Web.WebPages;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class AllergiesController : BaseController
    {
        private readonly AllergiesService _allergiesService = new AllergiesService(new ApplicationDbContext());

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_allergiesService.Read().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, AllergiesVm model)
        {
            if (model != null && ModelState.IsValid)
            {
                var success = _allergiesService.Create(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, AllergiesVm model)
        {
            if (model != null && ModelState.IsValid)
            {
                var success = _allergiesService.Update(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, AllergiesVm model)
        {
            if (model != null)
            {
                var success = _allergiesService.Destroy(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetAllAvailable(string text, int? id)
        {
            return _allergiesService.GetAllAvailableAllergies(text, id);
        }
    }
}