﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Controllers;
using e_Card.Helpers;
using e_Card.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class DoctorsController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly DoctorService _doctorService = new DoctorService(new ApplicationDbContext());

        // GET: AdministrationClinic/Doctors

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(new DoctorVm());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View("Create", new DoctorVm());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Update(string id)
        {
            var doctor = _db.Users.OfType<Doctor>().FirstOrDefault(d => d.Id == id && d.IsActive);

            if (doctor != null)
            {
                var doctorVm = new DoctorVm
                {
                    Name = doctor.Name,
                    Surname = doctor.Surname,
                    Email = doctor.Email,
                    Gender = doctor.Gender,
                    MiddleName = doctor.MiddleName,
                    Birth = doctor.Birth,
                    Nationality = doctor.Nationality,
                    Address = doctor.Address,
                    Profession = doctor.Profession,
                    DoctorId = doctor.Id,
                    UrlImage = doctor.UrlImage,
                    DateOfEmployment = doctor.DateOfEmployment,
                    DepartmentId = doctor.DepartmentId,
                    ClinicId = doctor.ClinicId
                };

                return View("Update", doctorVm);
            }
            return HttpNotFound();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_doctorService.Read().ToDataSourceResult(request));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DoctorHandler([DataSourceRequest] DataSourceRequest request, DoctorVm model, string status)
        {
            var success = false;
            if (model != null && ModelState.IsValid)
            {
                var accountController =
                    new AccountController {ControllerContext = ControllerContext};

                success = accountController.DoctorHandler(model, status);
            }

            return RedirectToAction(success ? "Index" : "Create");
        }

        public JsonResult GetDepartments(string text)
        {
            var department = _db.Departments
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!String.IsNullOrEmpty(text))
            {
                department = department.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(department.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase image)
        {
            if (image != null)
            {
                var extension = Path.GetExtension(image.FileName)?.ToLower();
                if (extension != ".jpg" && extension != ".png" && extension != ".jpeg")
                    return Json(new { success = false, error = "Wrong photo extension, please try with another photo." });

                Stream stream = image.InputStream;
                Image img = Image.FromStream(stream);
                var guid = Guid.NewGuid();
                
                var fileName = guid + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/UserImages"), fileName);

                //bool size300 = img.Height > 200 || img.Width < 200;

                //if (size300) return Json(new { success = false, message = "Photo dimensions should be minimum 200x200." });

                var myArray = (byte[])new ImageConverter().ConvertTo(img, typeof(byte[]));
                FileHelpers.SaveFile(myArray, physicalPath);

                return Json(new { success = true, path = "/UserImages/" + fileName, name = fileName});
            }

            return Json(new {success = false});
        }

        public JsonResult GetAll(string text)
        {
            
            var result = _db.Users.OfType<Doctor>()
                .Where(x => x.IsActive)
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!String.IsNullOrEmpty(text))
            {
                result = result.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(result.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllDoctors(string text)
        {
            var userId = User.Identity.GetUserId();
            var user = _db.MedicalStaff.Single(x => x.Id == userId);

            var result = _db.Users.OfType<Doctor>()
                .Where(x => x.IsActive && x.DepartmentId == user.DepartmentId)
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!String.IsNullOrEmpty(text))
            {
                result = result.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(result.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
