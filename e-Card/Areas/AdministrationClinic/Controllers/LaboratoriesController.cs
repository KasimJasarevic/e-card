﻿using System;
using System.Web.Mvc;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Linq;
using System.Web.WebPages;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class LaboratoriesController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly LaboratoryService _laboratoryService = new LaboratoryService(new ApplicationDbContext());

        // GET: AdministrationClinic/Laboratory
        [Authorize(Roles = "Admin")]
        // GET: AdministrationClinic/Diseases
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_laboratoryService.Read().ToDataSourceResult(request));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, LaboratoryVm model)
        {
            if (model != null && ModelState.IsValid)
            {
                var success = _laboratoryService.Create(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, LaboratoryVm model)
        {
            //var validate = _db.LaboratoryMedicalReports.Any(x => x.LaboratoryId == model.LaboratoryId);
            //if (validate)
            //{
            //     ModelState["LaboratoryReportIds"].Errors.Clear();

            //}
            if (model != null && ModelState.IsValid)
            {
               var success = _laboratoryService.Update(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, LaboratoryVm model)
        {
            if (model != null)
            {
               var success = _laboratoryService.Destroy(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult GetLaboratories(string text)
        {
            var lab = _db.Laboratories
                .Where(x => x.IsActive)
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!String.IsNullOrEmpty(text))
            {
                lab = lab.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(lab.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}