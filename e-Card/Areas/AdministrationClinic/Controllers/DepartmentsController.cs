﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class DepartmentsController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly DepartmentService _departmentService = new DepartmentService(new ApplicationDbContext());

        [Authorize(Roles = "Admin")]
        // GET: AdministrationClinic/Diseases
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_departmentService.Read().ToDataSourceResult(request));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, DepartmentVm model)
        {
            if (model != null && ModelState.IsValid)
            {
               var success = _departmentService.Create(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DepartmentVm model)
        {
            if (model != null && ModelState.IsValid)
            {
               var success = _departmentService.Update(model);

                if (!success.Equals(string.Empty))
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, DepartmentVm model)
        {
            if (model != null)
            {
                var success = _departmentService.Destroy(model);
                if (!success.Equals(string.Empty))
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetCascadeDepartment(int clinicId, string clinicText)
        {
            var res = _db.Departments
                .Where(x => x.Clinics.Select(y => y.ClinicId).Contains(clinicId) && x.IsActive)
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!String.IsNullOrEmpty(clinicText))
            {
                res = res.Where(g => g.Name.ToLower().StartsWith(clinicText.ToLower()));
            }

            return Json(res.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}