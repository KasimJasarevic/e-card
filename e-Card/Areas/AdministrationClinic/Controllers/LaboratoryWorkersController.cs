﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Controllers;
using e_Card.Helpers;
using e_Card.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class LaboratoryWorkersController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly LaboratoryWorkersService _laboratoryWorkersService = new LaboratoryWorkersService(new ApplicationDbContext());

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(new LaboratoryWorkerVm());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View("Create", new LaboratoryWorkerVm());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Update(string id)
        {
            var target = _db.Users.OfType<LaboratoryWorker>().FirstOrDefault(d => d.Id == id);

            if (target != null)
            {
                var labVm = new LaboratoryWorkerVm()
                {
                    LaboratoryWorkerId = target.Id,
                    Name = target.Name,
                    Surname = target.Surname,
                    Email = target.Email,
                    Gender = target.Gender,
                    MiddleName = target.MiddleName,
                    Birth = target.Birth,
                    Nationality = target.Nationality,
                    Address = target.Address,
                    LaboratoryId = target.LaboratoryId,
                    UrlImage = target.UrlImage,
                    DateOfEmployment = target.DateOfEmployment,
                };

                return View("Update", labVm);
            }
            return HttpNotFound();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_laboratoryWorkersService.Read().ToDataSourceResult(request));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LaboratoryWorkerHandler([DataSourceRequest] DataSourceRequest request, LaboratoryWorkerVm model, string status)
        {
            var success = false;
            if (model != null && ModelState.IsValid)
            {
                var accountController =
                    new AccountController { ControllerContext = ControllerContext };

                success = accountController.LaboratoryWorkerHandler(model, status);
            }

            return RedirectToAction(success ? "Index" : "Create");
        }

        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase image)
        {
            if (image != null)
            {
                var extension = Path.GetExtension(image.FileName)?.ToLower();
                if (extension != ".jpg" && extension != ".png" && extension != ".jpeg")
                    return Json(new { success = false, error = "Wrong photo extension, please try with another photo." });

                Stream stream = image.InputStream;
                Image img = Image.FromStream(stream);
                var guid = Guid.NewGuid();

                var fileName = guid + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/UserImages"), fileName);

                var myArray = (byte[])new ImageConverter().ConvertTo(img, typeof(byte[]));
                FileHelpers.SaveFile(myArray, physicalPath);

                return Json(new { success = true, path = "/UserImages/" + fileName, name = fileName });
            }

            return Json(new { success = false });
        }
    }
}