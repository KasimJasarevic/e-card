﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.SqlServer.Server;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class LabItemsController : BaseController, IDisposable
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        private readonly LabItemService _labItemService = new LabItemService(new ApplicationDbContext());

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_labItemService.Read().ToDataSourceResult(request));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, LabItemVm model)
        {
            if (model != null && ModelState.IsValid)
            {
               var success = _labItemService.Create(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, LabItemVm model)
        {
            if (model != null && ModelState.IsValid)
            {
                var success = _labItemService.Update(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, LabItemVm model)
        {
            if (model != null)
            {
                var success =_labItemService.Destroy(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetAll(string text)
        {
            var reports = _db.LaboratoryReports
                .Where(l => l.IsActive)
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                reports = reports.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(reports.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult CascadeLabItems(string text, int? laboratoryId)
        {
            var results = (from s in _db.LaboratoryReports
                where (from sa in _db.LaboratoryMedicalReports
                          where sa.LaboratoryReportId.Equals(s.Id) && sa.LaboratoryId.Equals(laboratoryId.Value)
                          select sa.LaboratoryReportId).Contains(s.Id) && s.IsActive
                select new
                {
                    s.Id,
                    s.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                results = results.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(results.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}