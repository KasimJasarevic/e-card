﻿using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class ClinicsController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly ClinicServices _clinicServices = new ClinicServices();

        // GET: RegistrationClinics/Clinics
        //[Authorize(Roles = "SuperAdmin")]
        public ActionResult Index()
        {
            return View(_db.Clinics.ToList());
        }

        //[Authorize(Roles = "SuperAdmin")]
        public ActionResult EditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_clinicServices.Read().ToDataSourceResult(request));
        }

        //[Authorize(Roles = "SuperAdmin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Create([DataSourceRequest] DataSourceRequest request, ClinicVm clinic)
        {
            if (clinic != null && ModelState.IsValid)
            {
                var success = _clinicServices.Create(clinic);
                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", @"Error ocured, please check input fields and try again");
                }
            }

            return Json(new[] { clinic }.ToDataSourceResult(request, ModelState));
        }

        //[Authorize(Roles = "SuperAdmin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, ClinicVm clinic)
        {
            if (clinic != null && ModelState.IsValid)
            {
                var success = _clinicServices.Update(clinic);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", @"Error ocured, please try again.");
                }
            }

            return Json(new[] { clinic }.ToDataSourceResult(request, ModelState));
        }

        //[Authorize(Roles = "SuperAdmin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, ClinicVm clinic)
        {
            if (clinic != null)
            {
                var success = _clinicServices.Destroy(clinic);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", @"Error ocured, please try again.");
                }
            }

            return Json(new[] { clinic }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult GetAll(string text, int? id, string status)
        {
            //if (id != null && (!status.IsNullOrWhiteSpace() && id.Value > 0))
            //{
            //    return GetDataForDepartments(text, id.Value);
            //}

            var clinics = _db.Clinics
                .Where(x => x.IsActive)
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                clinics = clinics.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(clinics.Take(10), JsonRequestBehavior.AllowGet);

        }

        //public JsonResult GetDataForDepartments(string text, int id)
        //{
        //    var results = (from s in _db.Clinics
        //        where (from sa in _db.DepartmentClinics
        //            where sa.ClinicId.Equals(s.Id) && sa.DepartmentId.Equals(id)
        //            select sa.ClinicId).Contains(s.Id) && s.IsActive
        //        select new
        //        {
        //            s.Id,
        //            s.Name
        //        });

        //    if (!string.IsNullOrEmpty(text))
        //    {
        //        results = results.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
        //    }

        //    return Json(results.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        //}
    }
}
