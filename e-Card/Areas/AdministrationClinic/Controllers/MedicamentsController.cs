﻿using System.Web.Mvc;
using System.Web.WebPages;
using e_Card.Areas.AdministrationClinic.Services;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace e_Card.Areas.AdministrationClinic.Controllers
{
    public class MedicamentsController : Controller
    {
        private readonly MedicamentService _medicamentService = new MedicamentService(new ApplicationDbContext());

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_medicamentService.Read().ToDataSourceResult(request));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, MedicamentVm model)
        {
            if (model != null && ModelState.IsValid)
            {
                var success = _medicamentService.Create(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, MedicamentVm model)
        {
            if (model != null && ModelState.IsValid)
            {
                var success = _medicamentService.Update(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, MedicamentVm model)
        {
            if (model != null)
            {
                var success = _medicamentService.Destroy(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetAll(string text)
        {
            return _medicamentService.GetAllMedicaments(text);
        }

        public JsonResult GetAllAvailable(string text, int? id)
        {
            return _medicamentService.GetAllAvailableMedicaments(text, id);
        }
    }
}