using System;
using System.Linq;
using e_Card.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace e_Card.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Context.Implementation.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
        protected override void Seed(Context.Implementation.ApplicationDbContext context)
        {
            //if (!context.Roles.Any(r => r.Name == "Admin"))
            //{
            //    var store = new RoleStore<IdentityRole>(context);
            //    var manager = new RoleManager<IdentityRole>(store);
            //    var role = new IdentityRole { Name = "Admin" };

            //    manager.Create(role);
            //}

            //if (!context.Roles.Any(r => r.Name == "Doctor"))
            //{
            //    var store = new RoleStore<IdentityRole>(context);
            //    var manager = new RoleManager<IdentityRole>(store);
            //    var role = new IdentityRole { Name = "Doctor" };

            //    manager.Create(role);
            //}

            //if (!context.Roles.Any(r => r.Name == "Patient"))
            //{
            //    var store = new RoleStore<IdentityRole>(context);
            //    var manager = new RoleManager<IdentityRole>(store);
            //    var role = new IdentityRole { Name = "Patient" };

            //    manager.Create(role);
            //}

            //if (!context.Roles.Any(r => r.Name == "LaboratoryWorker"))
            //{
            //    var store = new RoleStore<IdentityRole>(context);
            //    var manager = new RoleManager<IdentityRole>(store);
            //    var role = new IdentityRole { Name = "LaboratoryWorker" };

            //    manager.Create(role);
            //}

            //if (!context.Roles.Any(r => r.Name == "MedicalStaff"))
            //{
            //    var store = new RoleStore<IdentityRole>(context);
            //    var manager = new RoleManager<IdentityRole>(store);
            //    var role = new IdentityRole { Name = "MedicalStaff" };

            //    manager.Create(role);
            //}

            //if (!context.Users.Any(u => u.UserName == "Admin"))
            //{
            //    var store = new UserStore<ApplicationUser>(context);
            //    var manager = new UserManager<ApplicationUser>(store);
            //    var user = new ApplicationUser
            //    {
            //        UserName = "Admin",
            //        Name = "Admin",
            //        Surname = "Admin",
            //        Birth = DateTime.Now
            //    };

            //    manager.Create(user, "admin!!");
            //    manager.AddToRole(user.Id, "Admin");
            //}
        }
    }
}
