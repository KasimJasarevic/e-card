﻿namespace e_Card.Interfaces.Implementation
{
    public abstract class Entity<T> : IEntity<T>
    {
        public T Id { get; set; }
        public bool IsActive { get; set; } = true;
    }
}