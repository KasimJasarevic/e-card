﻿using System;
using System.ComponentModel.DataAnnotations;

namespace e_Card.Interfaces.Implementation
{
    public abstract class AuditableEntity<T> : Entity<T>, IAuditableEntity
    {
        [ScaffoldColumn(false)]
        public DateTime? CreatedDate { get; set; }

        //[MaxLength(256)]
        //[ScaffoldColumn(false)]
        //public string CreatedById { get; set; }

        //[ForeignKey("CreatedById")]
        //public ApplicationUser CreatedBy { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? UpdatedDate { get; set; }

        //[MaxLength(256)]
        //[ScaffoldColumn(false)]
        //public string UpdatedById { get; set; }

        //[ForeignKey("UpdatedById")]
        //public ApplicationUser UpdatedBy { get; set; }
    }
}