﻿namespace e_Card.Interfaces
{
    public interface IEntity<T>
    {
        T Id { get; set; }

        bool IsActive { get; set; }
    }
}
