﻿using System;

namespace e_Card.Interfaces
{
    public interface IAuditableEntity
    {
        DateTime? CreatedDate { get; set; }

        //string CreatedById { get; set; }

        //ApplicationUser CreatedBy { get; set; }

        DateTime? UpdatedDate { get; set; }

        //string UpdatedById { get; set; }

        //ApplicationUser UpdatedBy { get; set; }
    }
}