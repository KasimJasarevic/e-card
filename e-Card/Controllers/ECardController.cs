﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages;
using e_Card.Context.Implementation;
using e_Card.Models;
using e_Card.Services;
using e_Card.ViewModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.Ajax.Utilities;

namespace e_Card.Controllers
{
    public class ECardController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly ECardService _eCardService = new ECardService(new ApplicationDbContext());
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MedicalHistory(string id)
        {
            if (!id.IsNullOrWhiteSpace())
            {
                var card = _db.ECards.FirstOrDefault(c => c.PatientId == id);
                var patient = _db.Users.OfType<Patient>().FirstOrDefault(d => d.Id == id && d.IsActive);

                var medHistory = new MedicalHistoryVm
                {
                    CardId = card?.Id,
                    FamilyDisease = card?.FamilyDiseases,
                    UrlImage = patient?.UrlImage,
                    PatientName = patient?.Name,
                    PatientSurname = patient?.Surname,
                    PatientBirth = patient?.Birth,
                    Address = patient?.Address,
                    Jmbg = card?.SerialNo,
                    BloodType = patient?.BloodType,
                    Height = patient?.Height,
                    Weight = patient?.Weight,
                    Profession = patient?.Email,
                    DiseasesCount = _db.CardDiseases.Count(x => x.CardId == card.Id),
                    MedicamentsCount = _db.CardMedicaments.Count(x => x.CardId == card.Id),
                    AllergiesCount = _db.CardAllergieses.Count(x => x.CardId == card.Id),
                };

                return View("MedicalHistory", medHistory);
            }

            return HttpNotFound();
        }

        [Authorize]
        public JsonResult SaveFamilyDisease(int id, string value)
        {
            var entity = _db.ECards.FirstOrDefault(c => c.Id == id);
            if (entity != null)
            {
                entity.FamilyDiseases = value;
                entity.UpdatedDate = DateTime.Now;
                _db.ECards.Attach(entity);
                _db.Entry(entity).State = EntityState.Modified;
                _db.SaveChanges();
                return Json(new { success = true});
            }
            return Json(new { success = false, message="Error ocured"});
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_eCardService.Read().ToDataSourceResult(request));
        }

        public ActionResult ReadCardDisease([DataSourceRequest] DataSourceRequest request, int id)
        {
            return Json(_eCardService.ReadCardDisease(id).ToDataSourceResult(request));
        }

        public ActionResult ReadCardMedicament([DataSourceRequest] DataSourceRequest request, int id)
        {
            return Json(_eCardService.ReadCardMedicament(id).ToDataSourceResult(request));
        }

        public ActionResult ReadCardAllergies([DataSourceRequest] DataSourceRequest request, int id)
        {
            return Json(_eCardService.ReadCardAllergies(id).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult CreateCardDisease([DataSourceRequest] DataSourceRequest request, CardDiseaseVm model, int id)
        {
            if (model != null && ModelState.IsValid)
            {
                model.CardId = id;
                var success = _eCardService.CreateCardDisease(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult CreateCardMedicaments([DataSourceRequest] DataSourceRequest request, CardMedicamentVm model, int id)
        {
            if (model != null && ModelState.IsValid)
            {
                model.CardId = id;
                var success = _eCardService.CreateCardMedicaments(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult CreateCardAllergies([DataSourceRequest] DataSourceRequest request, CardAllergiesVm model, int id)
        {
            if (model != null && ModelState.IsValid)
            {
                model.CardId = id;
                var success = _eCardService.CreateCardAllergies(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult UpdateCardDisease([DataSourceRequest] DataSourceRequest request, CardDiseaseVm model,
            int id)
        {
            if (model != null && ModelState.IsValid)
            {
                model.CardId = id;
                var success = _eCardService.UpdateCardDisease(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] {model}.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult UpdateCardMedicaments([DataSourceRequest] DataSourceRequest request, CardMedicamentVm model,
            int id)
        {
            if (model != null && ModelState.IsValid)
            {
                model.CardId = id;
                var success = _eCardService.UpdateCardMedicaments(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult UpdateCardAllergies([DataSourceRequest] DataSourceRequest request, CardAllergiesVm model,
            int id)
        {
            if (model != null && ModelState.IsValid)
            {
                model.CardId = id;
                var success = _eCardService.UpdateCardAllergies(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyCardDisease([DataSourceRequest] DataSourceRequest request, CardDiseaseVm model,
            int id)
        {
            if (model != null)
            {
                model.CardId = id;
                var success = _eCardService.DestroyCardDisease(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] {model}.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyCardMedicaments([DataSourceRequest] DataSourceRequest request, CardMedicamentVm model,
            int id)
        {
            if (model != null)
            {
                model.CardId = id;
                var success = _eCardService.DestroyCardMedicaments(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyCardAllergies([DataSourceRequest] DataSourceRequest request, CardAllergiesVm model,
            int id)
        {
            if (model != null)
            {
                model.CardId = id;
                var success = _eCardService.DestroyCardAllergies(model);

                if (!success.IsEmpty())
                {
                    ModelState.AddModelError("", success);
                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
    }
}