﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_Card.Context.Implementation;
using e_Card.Helpers;
using e_Card.Models;
using e_Card.ViewModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using Notification = e_Card.Models.Notification;

namespace e_Card.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        [Authorize]
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "Doctors", new { area = "AdministrationClinic" });
            }
            if (User.IsInRole("Patient"))
            {
                return RedirectToAction("PatientInterface", "Home");
            }

            if (User.IsInRole("MedicalStaff"))
            {
                return RedirectToAction("Index", "TaskReview");
            }

            if (User.IsInRole("Doctor"))
            {
                return RedirectToAction("DoctorDashboard", "Home");
            }

            if (User.IsInRole("LaboratoryWorker"))
            {
                return RedirectToAction("LaboratoryDashboard", "Home");
            }

            return RedirectToAction("Login", "Account");
        }

        public ActionResult Chat()
        {
            return View();
        }

        [Authorize(Roles = "Patient")]
        public ActionResult PatientInterface()
        {
            var userId = User.Identity.GetUserId();
            var patient = _db.Users.OfType<Patient>().FirstOrDefault(x => x.Id == userId && x.IsActive);

            if (patient != null)
            {
                var patientVm = new PatientDashboardVm
                {
                    UrlImage = patient.UrlImage,
                    Name = patient.Name,
                    Surname = patient.Surname,
                    FullName = patient.FullName,
                    Jmbg = patient.Jmbg,
                    Weight = patient.Weight,
                    Height = patient.Height,
                    Address = patient.Address,
                    BloodType = patient.BloodType,
                    Birth = patient.Birth,
                    RequestVm = new RequestVm()
                };

                var firstOrDefault = _db.ECards.FirstOrDefault(x => x.PatientId == patient.Id && patient.IsActive);
                if (firstOrDefault != null)
                {
                    var cardId = firstOrDefault.Id;
                    patientVm.Medicamentses = _db.CardMedicaments.Where(x => x.CardId == cardId && x.IsActive).ToList();

                    patientVm.Therapies = _db.RequestTherapies.Include("Review")
                        .Where(x => x.Review.Request.PatientId == userId &&
                        DateTime.Today >= x.DateFrom &&
                        DateTime.Today <= x.DateTo).ToList();
                }

                return View("PatientInterface", patientVm);
            }
            return View("Index");
        }

        public ActionResult SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            // Save culture in a cookie
            var cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture")
                {
                    Value = culture,
                    Expires = DateTime.Now.AddYears(1)
                };
            }
            Response.Cookies.Add(cookie);
            if (Request.UrlReferrer != null) return Redirect(Request.UrlReferrer.ToString());

            return View("Index", Request.RequestContext.RouteData.Values["controller"].ToString());
        }

        [Authorize]
        [HttpPost]
        public JsonResult CreateRequest(RequestVm model)
        {
            if (ModelState.IsValid)
            {
                var patientId = User.Identity.GetUserId();
                //var doctorId = _db.Doctors.FirstOrDefault(x => x.DepartmentId == model.DepartmentId)?.Id;

                var entity = new Request
                {
                    DateRequest = DateTime.Now,
                    PreferedDate = model.PreferedDate,
                    Description = model.Description,
                    ShortDescription = model.ShortDescription,
                    DepartmentId = model.DepartmentId,
                    PatientId = patientId,
                    ClinicId = model.ClinicId
                };

                _db.Requests.Add(entity);
                _db.SaveChanges();

                return Json(new { success = true },
                    JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, message = "Something went wrong, please try again." },
                JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateRequestManual(RequestVm model)
        {
            var medicalStaff = User.Identity.GetUserId();
            var id = _db.Users.OfType<MedicalStaff>().FirstOrDefault(x => x.Id == medicalStaff);

            if (id == null)
                return Json(new { success = false });

            if (ModelState.IsValid)
            {
                var entity = new Request
                {
                    DateRequest = DateTime.Now,
                    PreferedDate = model.PreferedDate,
                    Description = model.Description,
                    ShortDescription = model.ShortDescription,
                    DepartmentId = id.DepartmentId,
                    PatientId = model.PatientId,
                    ClinicId = id.ClinicId
                };

                _db.Requests.Add(entity);
                _db.SaveChanges();

                return RedirectToAction("CreateSchedulerEvent", "TaskReview", new { id = entity.Id });
            }
            return Json(new { success = false, message = "Something went wrong, please try again." },
                JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllRequests(string text)
        {
            var results = _db.Requests
                .Where(x => x.IsActive)
                .Select(g => new
                {
                    g.Id,
                    g.Patient.Name
                });

            if (!string.IsNullOrEmpty(text))
            {
                results = results.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(results.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNotification()
        {
            var userId = User.Identity.GetUserId();

            if (userId != null)
            {
                var notifications = _db.Notifications.Where(x => x.PatientId == userId && x.IsActive).ToList();

                return Json(new { result = notifications }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = new List<Notification>() });
        }

        public ActionResult DoctorDashboard()
        {
            return View("DoctorDashboard");
        }

        public ActionResult PatientReview(int? requestId)
        {
            if (requestId != null)
            {
                //var request = _db.Requests.FirstOrDefault(x => x.Id == requestId.Value);
                //var patient = _db.Users.OfType<Patient>().FirstOrDefault(y => y.Id == request.PatientId);
                var patient = (from p in _db.Users.OfType<Patient>()
                               join r in _db.Requests on p.Id equals r.PatientId
                               where r.Id == requestId.Value
                               select p).FirstOrDefault();

                var patientReview = new PatientReviewVm
                {
                    ReviewId = requestId.Value,
                    Patient = patient
                };

                return RedirectToAction("ReviewDashboard", patientReview);
            }

            return HttpNotFound();
        }

        public ActionResult ReviewDashboard(int? requestId)
        {
            if (requestId == null) return HttpNotFound();

            // var ther = _db.Therapies.Include("Requests");

            var patient = (from p in _db.Users.OfType<Patient>()
                           join r in _db.Requests on p.Id equals r.PatientId
                           where r.Id == requestId.Value
                           select p).FirstOrDefault();

            var doctorId = User.Identity.GetUserId();
            var doctor = _db.Users.OfType<Doctor>().Include("Department").Single(x => x.Id == doctorId);

            var clinicId = _db.Requests.FirstOrDefault(x => x.Id == requestId.Value)?.ClinicId;
            var clinic = _db.Clinics.Single(x => x.Id == clinicId);

            var patientReview = new PatientReviewVm
            {
                ReviewId = requestId.Value,
                Patient = patient,
                Doctor = doctor,
                Clinic = clinic,
                PercriptionVm = new PercriptionVm()
                {
                    ReviewId = requestId.Value,
                    PatientId = patient?.Id
                },
                LaboratoryRequestVm = new LaboratoryRequestVm()
                {
                    ReviewId = requestId.Value,
                    PatientId = patient?.Id,
                },
                DiagnosisVm = new DiagnosisVm()
                {
                    ReviewId = requestId.Value,
                    PatientId = patient?.Id,
                    RequestTherapyVm = new RequestTherapyVm()
                    {
                        ReviewId = requestId.Value,
                        PatientId = patient?.Id
                    },
                    AllergieVm = new AllergieVm()
                    {
                        PatientId = patient?.Id
                    }
                }
            };

            return View("PatientReview", patientReview);
        }

        [HttpPost]
        public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        [HttpPost]
        [Authorize(Roles = "Doctor")]
        public JsonResult CreatePerscription(PercriptionVm model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var perscription = new Perscription
                    {
                        Quantity = model.Quantity,
                        ConsumptionWay = model.ConsumptionWay,
                        MedicamentId = model.MedicamentId,
                        ReviewId = model.ReviewId
                    };

                    var cardId = _db.ECards.Single(x => x.PatientId == model.PatientId).Id;

                    var isExist =
                        _db.CardMedicaments.FirstOrDefault(
                            x => x.CardId == cardId && x.MedicamentId == model.MedicamentId);
                    if (isExist == null)
                    {
                        var entity = new CardMedicament
                        {
                            CardId = cardId,
                            MedicamentId = model.MedicamentId,
                            DateFrom = DateTime.Now,
                            DateTo = null,
                            Comment = model.Note,
                            IsActive = true
                        };

                        _db.CardMedicaments.Add(entity);
                        _db.SaveChanges();
                    }

                    _db.Perscriptions.Add(perscription);

                    _db.SaveChanges();
                    return Json(new { success = true, data = perscription });
                }
                catch (Exception e)
                {
                    return Json(new { success = false });
                }
            }
            return Json(new { success = false });
        }

        [HttpPost]
        [Authorize(Roles = "Doctor")]
        public JsonResult CreateLaboratoryRequest(LaboratoryRequestVm model)
        {
            if (ModelState.IsValid)
            {
                var report = _db.LaboratoryReports.Where(x => model.LaboratoryReportIds.Contains(x.Id)).ToList();
                var request = new LaboratoryRequest
                {
                    ReviewId = model.ReviewId,
                    CommentDoctor = model.CommentDoctor,
                    LaboratoryReports = report,
                    LaboratoryId = model.LaboratoryId
                };

                _db.LaboratoryRequests.Add(request);
                _db.SaveChanges();

                return Json(new { success = true, data = request });
            }
            return Json(new { success = false });
        }

        public ActionResult ReadAllRequest(int? requestId)
        {
            if (requestId != null)
            {
                var result = _db.LaboratoryRequests.FirstOrDefault(x => x.Id == requestId.Value);

                var laboratoryReports = _db.LabReportses.Where(x => x.RequestId == requestId.Value).ToList();

                return Json(new { reports = result.LaboratoryReports, reportsFromWorker = laboratoryReports },
                    JsonRequestBehavior.AllowGet);
            }

            return Json(new { });
        }

        [Authorize]
        public JsonResult CreateRequestTherapy(RequestTherapyVm model)
        {
            if (ModelState.IsValid)
            {
                var therapy = new Therapy()
                {
                    Name = model.TherapyName,
                    IsActive = true,
                };

                _db.Therapies.Add(therapy);
                _db.SaveChanges();

                var thRequest = new RequestTherapy()
                {
                    ReviewId = model.ReviewId,
                    TherapyId = therapy.Id,
                    DiseaseId = model.DiseaseId,
                    DateFrom = model.DateFrom,
                    DateTo = model.DateTo,
                    Description = model.Description
                };

                _db.RequestTherapies.Add(thRequest);
                _db.SaveChanges();


                return Json(new { success = true, name = therapy.Name, dateFrom = thRequest.DateFrom, dateTo = thRequest.DateTo });
            }
            return Json(new { success = false });
        }

        [Authorize(Roles = "Doctor")]
        public JsonResult CreateAllergie(AllergieVm model)
        {
            if (ModelState.IsValid)
            {
                var allergie = new Allergies()
                {
                    Name = model.Name,
                    Description = model.Description
                };

                _db.Allergies.Add(allergie);
                _db.SaveChanges();

                var cardId = _db.ECards.Single(x => x.PatientId == model.PatientId).Id;

                var cardAllergies = new CardAllergies()
                {
                    CardId = cardId,
                    AllergieId = allergie.Id,
                    DateFrom = model.DateFromAllergie,
                    Comment = model.Description
                };

                _db.CardAllergieses.Add(cardAllergies);
                _db.SaveChanges();
                return Json(new { succes = true });
            }

            return Json(new { success = false });
        }

        [Authorize(Roles = "Doctor")]
        public JsonResult CreateDiagnosis(DiagnosisVm model)
        {
            if (ModelState.IsValid)
            {
                var diagnose = new Diagnosis()
                {
                    ReviewId = model.ReviewId,
                    DiseaseId = model.DiseaseDiagnosisId,
                    Description = model.Description,
                    ClinicalStatus = model.ClinicalStatus,
                    Advice = model.Advice
                };

                _db.Diagnoses.Add(diagnose);
                _db.SaveChanges();

                var cardId = _db.ECards.Single(x => x.PatientId == model.PatientId).Id;

                var isExist =
                    _db.CardDiseases.FirstOrDefault(
                        x => x.CardId == cardId && x.DiseaseId == model.DiseaseDiagnosisId);
                if (isExist == null)
                {
                    var cardDiagnose = new CardDisease()
                    {
                        CardId = cardId,
                        DiseaseId = model.DiseaseDiagnosisId,
                        DateFrom = DateTime.Now,
                        Comment = model.Description,
                        IsActive = true
                    };

                    _db.CardDiseases.Add(cardDiagnose);
                    _db.SaveChanges();
                }

                return Json(new { success = true });
            }

            return Json(new { success = false });
        }

        public ActionResult LaboratoryDashboard()
        {
            return View("LaboratoryDashboard");
        }

        public ActionResult GetLaboratoryRequest([DataSourceRequest] DataSourceRequest request)
        {
            var userId = User.Identity.GetUserId();
            var user = _db.LaboratoryWorkers.Include("Laboratory").Single(x => x.Id == userId);

            var labId = user.LaboratoryId;

            IList<LaboratoryRequestVm> result = _db.LaboratoryRequests.Where(x => x.IsActive
            && x.LaboratoryId == labId && x.LaboratoryReports.Any()).OrderByDescending(x => x.Id).Select(d => new LaboratoryRequestVm()
            {
                LaboratoryRequestId = d.Id,
                ReviewId = d.ReviewId,
                Patient = d.Review.Request.Patient,
                LaboratoryWorkerId = userId,
                CommentDoctor = d.CommentDoctor,
            }).ToList();

            return Json(result.ToDataSourceResult(request));
        }

        public ActionResult LaboratoryReportPage1(LaboratoryRequest model)
        {
            return View("LaboratoryReportPage", model);
        }

        [HttpGet]
        public ActionResult GoToLaboratoryRequest(int? requestId)
        {
            if (requestId != null)
            {
                var labRequest = _db.LaboratoryRequests.Include("LaboratoryReports")
                    .Single(x => x.Id == requestId.Value);

                var patient = _db.Reviews.Include("Request").Single(x => x.RequestId == labRequest.ReviewId).Request.Patient;
                var laboratory = _db.Laboratories.Single(x => x.Id == labRequest.LaboratoryId);

                var labRequest1 = new LaboratoryRequestVm()
                {
                    Laboratory = laboratory,
                    ReviewId = labRequest.ReviewId,
                    LaboratoryRequestId = labRequest.Id,
                    Patient = patient,
                    CommentDoctor = labRequest.CommentDoctor,
                    LaboratoryReports = labRequest.LaboratoryReports
                };

                return View("LaboratoryReportPage", labRequest1);
            }
            return HttpNotFound();
        }

        [Authorize(Roles = "LaboratoryWorker")]
        public JsonResult PassReports(List<Obj> objs, string s, int? id)
        {
            if (id != null)
            {
                var request = _db.LaboratoryRequests.Single(x => x.Id == id.Value);

                request.CommentLaboratoryWorker = s;
                request.LaboratoryWorkerId = User.Identity.GetUserId();
                request.Status = true;

                foreach (var item in objs)
                {
                    var labReports = new LabReports()
                    {
                        LaboratoryReportId = int.Parse(item.Id),
                        Value = item.Value,
                        RequestId = id.Value
                    };

                    _db.LabReportses.Add(labReports);
                    _db.SaveChanges();
                }

                _db.LaboratoryRequests.Attach(request);
                _db.Entry(request).State = EntityState.Modified;
                _db.SaveChanges();
            }
            return Json(new { success = true });
        }

        [Authorize]
        public ActionResult DeleteRequest(int? id)
        {
            if (id != null)
            {
                var r = _db.LaboratoryRequests.Single(x => x.Id == id.Value);
                if (r != null)
                {
                    r.IsActive = false;
                    _db.LaboratoryRequests.Attach(r);
                    _db.Entry(r).State = EntityState.Modified;
                    _db.SaveChanges();

                    return RedirectToAction("Index", "Home");
                }
            }
            return HttpNotFound();
        }

        [Authorize]
        public ActionResult FinishReview(int? reviewId)
        {
            if (reviewId != null)
            {
                var r = _db.TaskReviews.Single(x => x.RequestId == reviewId.Value);
                if (r != null)
                {
                    r.IsActive = false;
                    _db.TaskReviews.Remove(r);
                    _db.SaveChanges();
                }
                var rr = _db.Reviews.Single(x => x.RequestId == reviewId.Value);
                if (rr != null)
                {
                    rr.IsActive = false;
                    _db.Reviews.Attach(rr);
                    _db.Entry(rr).State = EntityState.Modified;
                    _db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            return HttpNotFound();
        }

        public JsonResult ReadNotification(int? id)
        {
            if (id != null)
            {
                var notification = _db.Notifications.Single(x => x.Id == id.Value);

                if (notification != null)
                {
                    notification.IsSeen = true;
                    _db.Notifications.Attach(notification);
                    _db.Entry(notification).State = EntityState.Modified;
                    _db.SaveChanges();

                    return Json(new {success = true});
                }
            }
            return Json(new {success = false});
        }
    }

    public class Obj
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }

    public class LabReportVm
    {
        public string Value { get; set; }
        public LaboratoryReport LaboratoryReports { get; set; }
    }
}