﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using e_Card.Context.Implementation;
using e_Card.Hubs;
using e_Card.Models;
using e_Card.Services;
using e_Card.ViewModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using Notification = e_Card.Models.Notification;

namespace e_Card.Controllers
{
    public class TaskReviewController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly SchedulerTaskReviewService _schedulerTaskReviewService = new SchedulerTaskReviewService();
        // GET: TaskReview
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var user = _db.Users.OfType<MedicalStaff>().FirstOrDefault(x => x.Id == userId && x.IsActive);

            var result = new TaskReviewVm
            {
                MedicalStaff = user,
            };

            return View(result);
        }

        public ActionResult SchedulerReview()
        {
            return View("SchedulerTask", new TaskReviewVm());
        }

        public ActionResult CreateSchedulerEvent(int? id)
        {
            if (id != null)
            {
                var request = _db.Requests.FirstOrDefault(x => x.Id == id.Value && x.IsActive);
                if (request != null)
                {
                    var taskVm = new TaskReviewVm
                    {
                        Description = request.Description,
                        RequestId = request.Id
                    };

                    request.Approved = true;
                    _db.Requests.Attach(request);
                    _db.Entry(request).State = EntityState.Modified;
                    _db.SaveChanges();

                   
                    return View("SchedulerTask", taskVm);
                }
            }

            return HttpNotFound();
        }

        public virtual JsonResult Meetings_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_schedulerTaskReviewService.GetAll().ToDataSourceResult(request));
        }

        public virtual JsonResult GetAllRequestsTherapy([DataSourceRequest] DataSourceRequest request)
        {
            var userId = User.Identity.GetUserId();
            var departmentId = _db.MedicalStaff.Single(x => x.Id == userId).DepartmentId;
            return Json(_schedulerTaskReviewService.GetAllTherapies(departmentId).ToDataSourceResult(request));
        }

        [Authorize]
        public virtual JsonResult ReadReview([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_schedulerTaskReviewService.GetAllDoctorReview(User.Identity.GetUserId()).ToDataSourceResult(request));
        }

        public virtual JsonResult Meetings_Destroy([DataSourceRequest] DataSourceRequest request,
            TaskReviewVm task, int? requestId)
        {
            if (ModelState.IsValid)
            {
                _schedulerTaskReviewService.Delete(task, ModelState, requestId);
            }

            return Json(new[] {task}.ToDataSourceResult(request, ModelState));
        }

        public virtual JsonResult Meetings_Create([DataSourceRequest] DataSourceRequest request,
            TaskReviewVm task, int? requestId)
        {
            if (ModelState.IsValid)
            {
                _schedulerTaskReviewService.Insert(task, ModelState, requestId);
            }

            return Json(new[] {task}.ToDataSourceResult(request, ModelState));
        }

        public virtual JsonResult Meetings_Update([DataSourceRequest] DataSourceRequest request,
            TaskReviewVm task, int? requestId)
        {
            if (ModelState.ContainsKey("RequestVm"))
                ModelState["RequestVm"].Errors.Clear();

            if (ModelState.IsValid)
            {
                _schedulerTaskReviewService.Update(task, ModelState, requestId);
            }

            return Json(new[] {task}.ToDataSourceResult(request, ModelState));
        }

        public ActionResult ApprovedRequests()
        {
            return View("ApprovedRequests", new TaskReviewVm());
        }

        public JsonResult ApproveRequest(int? id)
        {
            if (id != null)
            {
                var request = _db.Requests.FirstOrDefault(x => x.Id == id.Value);
                if (request != null)
                {
                    request.Approved = true;
                    _db.Requests.Attach(request);
                    _db.Entry(request).State = EntityState.Modified;
                    _db.SaveChanges();

                    var userName = request.Patient.UserName;

                    var notification = new Notification()
                    {
                        PatientId = request.PatientId,
                        IsSeen = false,
                        Remark = "Naručeni ste " + request.PreferedDate.ToShortDateString() + " na odjel " + request.Department.Name
                    };

                    _db.Notifications.Add(notification);
                    _db.SaveChanges();

                    NotificationHub.SendToUser(notification.Remark, notification.Id, userName);

                    //Send notification
                }

                return Json(new {success = true});
            }

            return Json(new {success = false});
        }

        public ActionResult Therapies()
        {
            return View("Therapies");
        }

        public JsonResult RejectRequest(int? id, string message)
        {
            if (id != null)
            {
                var request = _db.Requests.FirstOrDefault(x => x.Id == id.Value);
                if (request != null)
                {
                    request.IsActive = false;
                    _db.Requests.Attach(request);
                    _db.Entry(request).State = EntityState.Modified;
                    _db.SaveChanges();

                    var userName = request.Patient.UserName;
                    var msg = !string.IsNullOrWhiteSpace(message) ? message : "";

                    var notification = new Notification()
                    {
                        PatientId = request.PatientId,
                        IsSeen = false,
                        Remark = "Vaš zahtjev za pregledom je odbijen.\n "
                                                            + "Poruka: " + msg 
                    };

                    _db.Notifications.Add(notification);
                    _db.SaveChanges();

                    NotificationHub.SendToUser(msg, notification.Id, userName);
                    //Send notification
                }

                return Json(new {success = true});
            }

            return Json(new {success = false});
        }
    }
}