﻿using System.Linq;
using System.Web.Mvc;
using e_Card.Areas.AdministrationClinic.ViewModel;
using e_Card.Context.Implementation;
using e_Card.Models;
using e_Card.ViewModel;
using Microsoft.AspNet.Identity;

namespace e_Card.Controllers
{
    public class MedicalStaffController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MedicallStaffDashboard()
        {
            var userId = User.Identity.GetUserId();
            var user = _db.Users.OfType<MedicalStaff>().FirstOrDefault(x => x.Id == userId && x.IsActive);

            if (user != null)
            {
                var userVm = new MedicalStaffVm
                {
                    UrlImage = user.UrlImage,
                    Name = user.Name,
                    Surname = user.Surname,
                    FullName = user.FullName,
                    //Jmbg = user.Jmbg,
                    //Weight = user.Weight,
                    //Height = user.Height,
                    Address = user.Address,
                    //BloodType = user.BloodType,
                    Birth = user.Birth,
                    //RequestVms = _db.Requests.Where(x => x.Doctor.DepartmentId == user.DepartmentId).Select(
                    //    x => new RequestVm()
                    //    {
                    //        Description = x.Description,
                    //        ShortDescription = x.ShortDescription,
                    //        DateRequest = x.DateRequest,
                    //        PreferedDate = x.PreferedDate,
                    //        Approved = x.Approved,
                    //        DepartmentId = x.Doctor.DepartmentId
                    //    }).ToList(),
                    //RequestVm = new RequestVm()
                };


                //var firstOrDefault = _db.ECards.FirstOrDefault(x => x.PatientId == patient.Id && patient.IsActive);
                //if (firstOrDefault != null)
                //{
                //    var cardId = firstOrDefault.Id;
                //    patientVm.Medicamentses = _db.CardMedicaments.Where(x => x.CardId == cardId && x.IsActive).ToList();

                //    // patientVm.Therapies = _db.RequestTherapies.FirstOrDefault(x => x.ReviewId)
                //}

                return View("MedicallStaffDashboard", new MedicalStaffVm());
            }
            return RedirectToAction("Index", "Home", new {area = ""});
        }
    }
}