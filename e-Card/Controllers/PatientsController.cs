﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_Card.Context.Implementation;
using e_Card.Helpers;
using e_Card.Models;
using e_Card.Services;
using e_Card.ViewModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace e_Card.Controllers
{
    public class PatientsController : BaseController
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();
        private readonly PatientService _patientService = new PatientService(new ApplicationDbContext());

        //[Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View(new PatientVm());
        }

        public ActionResult Create()
        {
            return View("Create", new PatientVm());
        }

        public ActionResult Update(string id)
        {
            var user = _db.Users.OfType<Patient>().FirstOrDefault(d => d.Id == id && d.IsActive);

            if (user != null)
            {
                var userVm = new PatientVm()
                {
                    Name = user.Name,
                    Surname = user.Surname,
                    Email = user.Email,
                    Gender = user.Gender,
                    MiddleName = user.MiddleName,
                    Birth = user.Birth,
                    Nationality = user.Nationality,
                    Address = user.Address,
                    UrlImage = user.UrlImage,
                    PatientId = user.Id,
                    Jmbg = user.Jmbg,
                    BloodType = user.BloodType,
                    Weight = user.Weight,
                    Height = user.Height,
                    //MedicamentIds = _db.ECards.FirstOrDefault(c => c.PatientId == user.Id)?.Medicaments.Select(m => m.MedicamentId).ToList(),
                    //DiseaseIds = _db.ECards.FirstOrDefault(d => d.PatientId == user.Id)?.Diseases.Select(dd => dd.DiseaseId).ToList()
                };

                return View("Update", userVm);
            }
            return HttpNotFound();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientHandler([DataSourceRequest] DataSourceRequest request, PatientVm model, string status)
        {
            var success = false;
            if (model != null && ModelState.IsValid)
            {
                var accountController =
                    new AccountController { ControllerContext = ControllerContext };

                success = accountController.PatientHandler(model, status);
            }

            if (success)
            {
                return RedirectToAction("PatientCard", "Patients");
            }

            return RedirectToAction("Create", "Patients");
        }

        public JsonResult GetAll(string text)
        {

            var result = _db.Users.OfType<Patient>()
                .Select(g => new
                {
                    g.Id,
                    g.Name
                });

            if (!String.IsNullOrEmpty(text))
            {
                result = result.Where(g => g.Name.ToLower().StartsWith(text.ToLower()));
            }

            return Json(result.Take(10).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase image)
        {
            if (image != null)
            {
                var extension = Path.GetExtension(image.FileName)?.ToLower();
                if (extension != ".jpg" && extension != ".png" && extension != ".jpeg")
                    return Json(new { success = false, error = "Wrong photo extension, please try with another photo." });

                Stream stream = image.InputStream;
                Image img = Image.FromStream(stream);
                var guid = Guid.NewGuid();

                var fileName = guid + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/UserImages"), fileName);

                //bool size300 = img.Height > 200 || img.Width < 200;

                //if (size300) return Json(new { success = false, message = "Photo dimensions should be minimum 200x200." });

                var myArray = (byte[])new ImageConverter().ConvertTo(img, typeof(byte[]));
                FileHelpers.SaveFile(myArray, physicalPath);

                return Json(new { success = true, path = "/UserImages/" + fileName, name = fileName });
            }

            return Json(new { success = false });
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_patientService.Read().ToDataSourceResult(request));
        }

        public ActionResult PatientCard(int? id)
        {
            return View("PatientCard");
        }
    }
}