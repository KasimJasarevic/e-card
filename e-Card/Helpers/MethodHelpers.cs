﻿using System;

namespace e_Card.Helpers
{
    public class MethodHelpers
    {
        public static string GetTimeStamp(DateTime value)
        {
            return value.ToString("yyyMMddHHmmssfff");
        }

        public static string GetTimeStamp()
        {
            return DateTime.Now.ToString("yyyMMddHHmmssfff");
        }
    }
}