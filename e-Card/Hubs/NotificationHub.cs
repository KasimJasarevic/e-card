﻿using Microsoft.AspNet.SignalR;

namespace e_Card.Hubs
{
    public class NotificationHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void SendNotifications(string message, string userId)
        {
            Clients.User(userId).broadcastMessage(message);
        }

        //public void SendToUser(string userId, string message)
        //{
        //    Clients.User(userId).broadcastMessage(message);
        //}

        internal static void SendToUser(string message, int notificationId, string userId)
        {
            // Call the addNewMessageToPage method to update clients.
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            context.Clients.User(userId).broadcastMessage(message, notificationId);
            //context.Clients.All.broadcastMessage(message);
        }
    }
}